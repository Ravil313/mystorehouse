﻿using DataAccessLayer;
using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;

var optionBuilder = new DbContextOptionsBuilder<DataContext>()
    .UseNpgsql("Host=localhost;Port=5432;Database=MyStoreHouse;Username=postgres;Password=12345");

var dataContext = new DataContext(optionBuilder.Options);

//Добавление товаров
dataContext.ProductsData.Add(new ProductData()
{
    ProductDataName = "Бананы",
    ProductDataArticle = 1,
    LastPurchasePrice = 15,
    LastSellPrice = 20
});
dataContext.ProductsData.Add(new ProductData()
{
    ProductDataName = "Киви",
    ProductDataArticle = 2,
    LastPurchasePrice = 35,
    LastSellPrice = 40
});
dataContext.ProductsData.Add(new ProductData()
{
    ProductDataName = "Апельсины",
    ProductDataArticle = 2,
    LastPurchasePrice = 25,
    LastSellPrice = 30
});
dataContext.SaveChanges();

//Добавление складов
dataContext.StoreHousesData.Add(new StoreHouseData(){StoreHouseName = "Склад 1" });
dataContext.StoreHousesData.Add(new StoreHouseData(){StoreHouseName = "Склад 2" });
dataContext.StoreHousesData.Add(new StoreHouseData(){StoreHouseName = "Склад 2" });
dataContext.SaveChanges();

//Добавление связей товаров и складов
dataContext.ProductsStoreHousesData.Add(new ProductStoreHouseData()
{
    ProductId = 1,
    StoreHouseId = 1,
    Count = 27
});
dataContext.ProductsStoreHousesData.Add(new ProductStoreHouseData()
{
    ProductId = 2,
    StoreHouseId = 2,
    Count = 27
});
dataContext.ProductsStoreHousesData.Add(new ProductStoreHouseData()
{
    ProductId = 3,
    StoreHouseId = 3,
    Count = 27
});
dataContext.SaveChanges();

//Добавление поступлений
dataContext.AdmissionsData.Add(new AdmissionData()
{
    AdmissionNumber = 1,
    StoreHouseId = 1,
    Date = "18.12.2022",
    AdmissionProducts = new List<AdmissionProductsData>()
    {
        new AdmissionProductsData()
        {
            AdmissionId = 1,
            ProductId = 1,
            Price = 15,
            Count = 30
        }
    }
});
dataContext.AdmissionsData.Add(new AdmissionData()
{
    AdmissionNumber = 2,
    StoreHouseId = 2,
    Date = "18.12.2022",
    AdmissionProducts = new List<AdmissionProductsData>()
    {
        new AdmissionProductsData()
        {
            AdmissionId = 2,
            ProductId = 2,
            Price = 35,
            Count = 30
        }
    }
});
dataContext.AdmissionsData.Add(new AdmissionData()
{
    AdmissionNumber = 3,
    StoreHouseId = 3,
    Date = "18.12.2022",
    AdmissionProducts = new List<AdmissionProductsData>()
    {
        new AdmissionProductsData()
        {
            AdmissionId = 3,
            ProductId = 3,
            Price = 25,
            Count = 30
        }
    }
});
dataContext.SaveChanges();

//Добавление продаж
dataContext.SellsData.Add(new SellData()
{
    SellNumber = 1,
    StoreHouseId = 1,
    Date = "18.12.2022",
    SellProducts = new List<SellProductsData>()
    {
        new SellProductsData()
        {
            SellId = 1,
            ProductId = 1,
            SellPrice = 20,
            Count = 3
        }
    }
});
dataContext.SellsData.Add(new SellData()
{
    SellNumber = 2,
    StoreHouseId = 2,
    Date = "18.12.2022",
    SellProducts = new List<SellProductsData>()
    {
        new SellProductsData()
        {
            SellId = 2,
            ProductId = 2,
            SellPrice = 40,
            Count = 3
        }
    }
});
dataContext.SellsData.Add(new SellData()
{
    SellNumber = 3,
    StoreHouseId = 3,
    Date = "18.12.2022",
    SellProducts = new List<SellProductsData>()
    {
        new SellProductsData()
        {
            SellId = 3,
            ProductId = 3,
            SellPrice = 30,
            Count = 3
        }
    }
});
dataContext.SaveChanges();