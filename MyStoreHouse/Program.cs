using System.Text;
using System.Text.Json.Serialization;
using CQRS.Admission.Commands;
using CQRS.CustomCheckers;
using CQRS.Moving.Commands;
using CQRS.Products.Queries.Mappers;
using CQRS.Products.Queries.Models;
using CQRS.Products.Queries.Validators;
using CQRS.Security;
using CQRS.Sell.Commands;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MyStoreHouse.Extensions;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddFluentValidationAutoValidation(); //включение автоматической валидации Fluent Validation
builder.Services.AddValidatorsFromAssemblyContaining(typeof(GetProductByArticleQueryValidator));
builder.Services.AddAutoMapper(typeof(ProductMapProfile));
builder.Services.AddMediatR(typeof(GetProductByArticleQuery));
builder.Services.AddDbContext<DataContext>(optionsBuilder =>
    optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=MyStoreHouse;Username=postgres;Password=12345"));
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true); //приведение даты к типу даты самой БД
builder.Services.AddControllersWithViews()
    .AddJsonOptions(x => x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles); //решение зацикливания загрузки
builder.Services.AddTransient<ICustomChecker<CreateMovingCommand>, MovingCommandChecker>();
builder.Services.AddTransient<ICustomChecker<CreateAdmissionCommand>, AdmissionCommandChecker>();
builder.Services.AddTransient<ICustomChecker<CreateSellCommand>, SellCommandChecker>();
builder.Services.AddTransient<IJWTProvider, JWTProvider>();
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    {
        options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
            In = ParameterLocation.Header,
            Description = "Please insert JWT with Bearer into field",
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey,
            BearerFormat = "JWT"
        });
        
        options.AddSecurityRequirement(new OpenApiSecurityRequirement()
        {
            {
                new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "Bearer"}
                },
                Array.Empty<string>()
            }
        });
        
        options.CustomSchemaIds(y => y.FullName);
    }
    {
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "MyStoreHouse.xml"));
    }
    {
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "DataAccessLayer.xml"));
    }
    {
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "CQRS.xml"));
    }
});
builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.RequireAuthenticatedSignIn = false;
}).AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("my-secret-key-my-secret-key-my-secret-key")),
        ValidateIssuer = false,
        ValidateAudience = false
    };
});

var app = builder.Build();

app.UsrLogUrl(); //логирование URL запроса

app.UseCustomExceptionHandler(); //обработчик исключений

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();