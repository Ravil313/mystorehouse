﻿using Microsoft.AspNetCore.Http.Extensions;

namespace MyStoreHouse.Middlewares;

/// <summary>
/// Middleware для логирования запросов
/// </summary>
public class LogUrlMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<LogUrlMiddleware> _logger;

    /// <summary>
    /// Внедрение зависимойтей
    /// </summary>
    /// <param name="loggerFactory">Логирование</param>
    /// <param name="next">Передатчик запросов</param>
    /// <exception cref="ArgumentNullException"></exception>
    public LogUrlMiddleware(ILoggerFactory loggerFactory, RequestDelegate next)
    {
        _logger = loggerFactory?.CreateLogger<LogUrlMiddleware>()??
                  throw new ArgumentNullException(nameof(loggerFactory));
        _next = next;
    }

    /// <summary>
    /// Логирование запроса
    /// </summary>
    /// <param name="context">Запрос</param>
    public async Task InvokeAsync(HttpContext context)
    {
        if (context.Request.Method == "GET")
        {
            _logger.LogInformation("Request URL: {RequestPath}", context.Request.GetDisplayUrl());
            await this._next(context);
        }
        else
        {
            await this._next(context);
        }
        
    }
}