﻿using System.Text.Json;
using CQRS.Exceptions;

namespace MyStoreHouse.Middlewares;

/// <summary>
/// Middleware для обработки исключений
/// </summary>
public class ExceptionsHandlerCustomMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<ExceptionsHandlerCustomMiddleware> _logger;


    /// <summary>
    /// Внедрение зависимойтей
    /// </summary>
    /// <param name="logger">Логирование</param>
    /// <param name="next">Передатчик запросов</param>
    public ExceptionsHandlerCustomMiddleware(ILogger<ExceptionsHandlerCustomMiddleware> logger, RequestDelegate next)
    {
        _logger = logger;
        _next = next;
    }

    /// <summary>
    /// Обработка исключений возникающих при выполнении запрса
    /// </summary>
    /// <param name="context">Запрос</param>
    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next.Invoke(context);
        }
        catch (NotFoundException e)
        {
            _logger.LogError(e.Message);
            context.Response.StatusCode = StatusCodes.Status404NotFound;
            context.Response.ContentType = "application/json";
            var response = JsonSerializer.Serialize(new { errors = e.Message });
            await context.Response.WriteAsync(response);

        }
        catch (SameObjectException e)
        {
            _logger.LogError(e.Message);
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            context.Response.ContentType = "application/json";
            var response = JsonSerializer.Serialize(new { errors = e.Message });
            await context.Response.WriteAsync(response);

        }
        catch (ArgumentOutOfRangeException e)
        {
            _logger.LogError(e.Message);
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            context.Response.ContentType = "application/json";
            var response = JsonSerializer.Serialize(new { errors = e.Message });
            await context.Response.WriteAsync(response);

        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            context.Response.ContentType = "application/json";
            var response = JsonSerializer.Serialize(new { errors = e.Message });
            await context.Response.WriteAsync(response);
        }
    }
}