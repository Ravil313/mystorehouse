﻿using CQRS.Admission.Commands;
using CQRS.Admission.Queries.Models;
using CQRS.Moving.Commands;
using CQRS.Moving.Queries.Models;
using CQRS.Sell.Commands;
using CQRS.Sell.Queries.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MyStoreHouse.Controllers;
/// <summary>
/// Контроллер запросов для документов (поступление, перемещение, продажа)
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class DocumentsController : ControllerBase
{
    private readonly IMediator _mediator;
    /// <summary>
    /// Внедрение зависимости от проекта CQRS
    /// </summary>
    /// <param name="mediator"></param>
    public DocumentsController(IMediator mediator)
    {
        _mediator = mediator;
    }
    /// <summary>
    /// Получение поступления по его номеру
    /// </summary>
    /// <param name="getAdmissionByNumberQuery">Номер поступления</param>
    /// <returns>Поступление из базы данных</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet("Admission number")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(AdmissionDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> GetAdmission([FromQuery]GetAdmissionByNumberQuery getAdmissionByNumberQuery)
    {
        var admissionDto = await _mediator.Send(getAdmissionByNumberQuery);
        return Ok(admissionDto);
    }
    /// <summary>
    /// Получение всех поступлений
    /// </summary>
    /// <returns>Коллекция поступлений из базы данных</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet("Admissions")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(IEnumerable<AdmissionDto>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult<IEnumerable<AdmissionDto>>> GetAllAdmissions()
    {
        var admissionsDto = await _mediator.Send(new GetAllAdmissionsQuery());
        return Ok(admissionsDto);
    }
    /// <summary>
    /// Создание поступления
    /// </summary>
    /// <param name="createAdmissionCommand">Новое поступление с заполненными полями</param>
    /// <returns>Идентификатор поступления</returns>
    [Authorize(Roles = "User, Admin")]
    [HttpPost("Admission")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> CreateAdmission([FromBody] CreateAdmissionCommand createAdmissionCommand)
    {
        var admissionId = await _mediator.Send(createAdmissionCommand);
        return Ok(admissionId);
    }
    /// <summary>
    /// Получение перемещения по его номеру
    /// </summary>
    /// <param name="getMovingByNumberQuery">Номер перемещения</param>
    /// <returns>Перемещение из базы данных</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet("Moving number")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(MovingDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> GetMoving([FromQuery]GetMovingByNumberQuery getMovingByNumberQuery)
    {
        var movingDto = await _mediator.Send(getMovingByNumberQuery);
        return Ok(movingDto);
    }
    /// <summary>
    /// Получение всех премещений
    /// </summary>
    /// <returns>Коллекция перемещений из базы данных</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet("Movings")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(IEnumerable<MovingDto>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult<IEnumerable<MovingDto>>> GetAllMoving()
    {
        var movingDto = await _mediator.Send(new GetAllMovingQuery());
        return Ok(movingDto);
    }
    /// <summary>
    /// Создание перемещения
    /// </summary>
    /// <param name="createMovingCommand">Новое перемещение с заполненными полями</param>
    /// <returns>Идентификатор перемещения</returns>
    [Authorize(Roles = "User, Admin")]
    [HttpPost("Moving")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> CreateMoving([FromBody] CreateMovingCommand createMovingCommand)
    {
        var movingId = await _mediator.Send(createMovingCommand);
        return Ok(movingId);
    }
    /// <summary>
    /// Получение продажи по номеру
    /// </summary>
    /// <param name="getSellByNumberQuery">Номер продажи</param>
    /// <returns>Продажа из базы данных</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet("Query number")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(SellDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> GetSell([FromQuery]GetSellByNumberQuery getSellByNumberQuery)
    {
        var sellDto = await _mediator.Send(getSellByNumberQuery);
        return Ok(sellDto);
    }
    /// <summary>
    /// Получение всех продаж
    /// </summary>
    /// <returns>Коллекция продаж из базы данных</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet("Sells")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(IEnumerable<SellDto>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult<IEnumerable<SellDto>>> GetAllSell()
    {
        var sellsDto = await _mediator.Send(new GetAllSellsQuery());
        return Ok(sellsDto);
    }
    /// <summary>
    /// Создание продажи
    /// </summary>
    /// <param name="createSellCommand">Новая продажа с заполненными полями</param>
    /// <returns>Идентификатор новой продажи</returns>
    [Authorize(Roles = "User, Admin")]
    [HttpPost("Sell")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> CreateSell([FromBody] CreateSellCommand createSellCommand)
    {
        var sellId = await _mediator.Send(createSellCommand);
        return Ok(sellId);
    }
}