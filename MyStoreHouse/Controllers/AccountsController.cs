﻿using CQRS.Accounts.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MyStoreHouse.Controllers;

/// <summary>
/// Аутентификация пользователя
/// </summary>
[Authorize]
[ApiController]
[Route("api/[controller]")]
public class AccountsController : ControllerBase
{
    private readonly IMediator _mediator;

    /// <summary>
    /// Внедрение зависимости
    /// </summary>
    /// <param name="mediator"></param>
    public AccountsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Возвращает JWT токен для аутентификации
    /// </summary>
    /// <param name="loginQuery">Имя и пароль пользователя</param>
    /// <returns>JWT токен</returns>
    [AllowAnonymous]
    [HttpGet("/login")]
    public async Task<ActionResult<string>> Login([FromQuery] LoginQuery loginQuery)
    {
        try
        {
            var jwtToken = await _mediator.Send(loginQuery);
            return Ok(jwtToken);
        }
        catch (Exception exception)
        {
            return BadRequest();
        }
    }
}