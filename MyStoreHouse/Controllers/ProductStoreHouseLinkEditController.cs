﻿using CQRS.ProductStoreHouseLinks.Commands.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MyStoreHouse.Controllers;
/// <summary>
/// Контроллер запросов для связей товара и склада
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class ProductStoreHouseLinkEditController : ControllerBase
{
    private readonly IMediator _mediator;
    /// <summary>
    /// Внедрение зависимости от проекта CQRS
    /// </summary>
    /// <param name="mediator"></param>
    public ProductStoreHouseLinkEditController(IMediator mediator)
    {
        _mediator = mediator;
    }
    /// <summary>
    /// Создание связи между товаром и складом
    /// </summary>
    /// <param name="createProductStoreHouseLink">Заполненные данные о новой связи</param>
    /// <returns>Сообщение о выполнении команды</returns>
    [Authorize(Roles = "User, Admin")]
    [HttpPost]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Create([FromBody] CreateProductStoreHouseLink createProductStoreHouseLink)
    {
        var productStoreHouseLink = await _mediator.Send(createProductStoreHouseLink);
        return Ok(productStoreHouseLink);
    }
    /// <summary>
    /// Редактирование связи между товаром и складом
    /// </summary>
    /// <param name="editProductStoreHouseLink">Заполненные данные о редактируемой связи и о новой связи</param>
    /// <returns>Сообщение о выполнении команды</returns>
    [Authorize(Roles = "Admin")]
    [HttpPut]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Edit([FromBody] EditProductStoreHouseLink editProductStoreHouseLink)
    {
        var product = await _mediator.Send(editProductStoreHouseLink);
        return Ok(product); 
    }
    /// <summary>
    /// Удаление связи между товаром и складом
    /// </summary>
    /// <param name="deleteProductStoreHouseLink">Идентификаторы связи</param>
    /// <returns>Сообщение о выполнении команды</returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Delete([FromQuery] DeleteProductStoreHouseLink deleteProductStoreHouseLink)
    {
        var product = await _mediator.Send(deleteProductStoreHouseLink);
        return Ok(product); 
    }
}