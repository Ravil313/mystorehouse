﻿using System.Security.Claims;
using CQRS.Users;
using CQRS.Users.Commands;
using CQRS.Users.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MyStoreHouse.Controllers;

/// <summary>
/// Контроллер запросов для пользователя
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class UserController : ControllerBase
{
    private readonly IMediator _mediator;

    /// <summary>
    /// Добавление зависимости от CQRS
    /// </summary>
    /// <param name="mediator"></param>
    public UserController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    /// <summary>
    /// Просмотр пользователя по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор пользователя</param>
    /// <returns>Данные пользователя</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet("{id:int}")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Get(int id)
    {
        var getUserQuery = new GetUserQuery() { Id = id };
        var userDto = await _mediator.Send(getUserQuery);
        return Ok(userDto);
    }

    /// <summary>
    /// Просмотр всех пользователей
    /// </summary>
    /// <returns>Список пользователей</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(typeof(IEnumerable<UserDto>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult<IEnumerable<UserDto>>> GetAll()
    {
        var userDto = await _mediator.Send(new GetUsersQuery());
        return Ok(userDto);
    }

    /// <summary>
    /// Добавление нового пользователя
    /// </summary>
    /// <param name="createUserCommand">Данные пользователя</param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpPost]
    [Produces("application/json")]
    [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Create([FromBody] CreateUserCommand createUserCommand)
    {
        var userId = await _mediator.Send(createUserCommand);
        return Ok(userId);
    }
    
    /// <summary>
    /// Изменяет данные пользователя
    /// </summary>
    /// <param name="editUserCommand">Старый идентификатор и овые данные пользователя</param>
    /// <returns>Имя пользователя</returns>
    [Authorize(Roles = "User, Admin")]
    [HttpPut]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Edit([FromBody] EditUserCommand editUserCommand)
    {
        var user =await _mediator.Send(editUserCommand);
        return Ok(user); 
    }
    
    /// <summary>
    /// Удаляет ползователя по идентфикатору
    /// </summary>
    /// <param name="id">Идентификатор пользователя</param>
    /// <returns></returns>
    [Authorize(Roles = "User, Admin")]
    [HttpDelete("{id:int}")] 
    [Produces("application/json")]
    [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Delete([FromRoute] int id)
    {
        int.TryParse(User.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Sid)?.Value, out var loggedUserId);
        if (id != loggedUserId)
        {
            return BadRequest("Неверный идентификатор");
        }
        var user = new DeleteUserCommand(){Id = id};
        await _mediator.Send(user);
        return Ok(user);
    }
}