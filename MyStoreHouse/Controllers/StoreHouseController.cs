﻿using CQRS.StoreHouse.Commands.Models;
using CQRS.StoreHouse.Queries.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MyStoreHouse.Controllers;
/// <summary>
/// Контроллер запросов для складов
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class StoreHouseController : ControllerBase
{
    private readonly IMediator _mediator;
    /// <summary>
    /// Внедрение зависимости от проекта CQRS
    /// </summary>
    /// <param name="mediator"></param>
    public StoreHouseController(IMediator mediator)
    {
        _mediator = mediator;
    }
    /// <summary>
    /// Получение склада по его идентификатору
    /// </summary>
    /// <param name="getStoreHouseByIdQuery">Идентификатор склада</param>
    /// <returns>Склад из базы данных</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet("Id")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(StoreHouseDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Get([FromQuery]GetStoreHouseByIdQuery getStoreHouseByIdQuery)
    {
        var storeDto = await _mediator.Send(getStoreHouseByIdQuery);
        return Ok(storeDto);
    }
    /// <summary>
    /// Получение всех складов
    /// </summary>
    /// <returns>Коллекция складов из базы данных</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(typeof(IEnumerable<StoreHouseDto>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult<IEnumerable<StoreHouseDto>>> GetAll()
    {
        var storesDto = await _mediator.Send(new GetAllStoreHousesQuery());
        return Ok(storesDto);
    }
    /// <summary>
    /// Команда создания склада
    /// </summary>
    /// <param name="createStoreHouseCommand">Новый склад с заполненными полями</param>
    /// <returns>Идентификатор склада</returns>
    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Produces("application/json")]
    [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Create([FromBody] CreateStoreHouseCommand createStoreHouseCommand)
    {
        var storeId = await _mediator.Send(createStoreHouseCommand);
        return Ok(storeId);
    }
    /// <summary>
    /// Команда редактирования склада
    /// </summary>
    /// <param name="editStoreHouseCommand">Склад с заполненными полями для редактирования данных</param>
    /// <returns>Сообщение о выполнении команды</returns>
    [Authorize(Roles = "Admin")]
    [HttpPut]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Edit([FromBody] EditStoreHouseCommand editStoreHouseCommand)
    {
        var store = await _mediator.Send(editStoreHouseCommand);
        return Ok(store); 
    }
    /// <summary>
    /// Удаление склада
    /// </summary>
    /// <param name="deleteStoreHouseCommand">Артикул удаляемого склада</param>
    /// <returns>Сообщение о выполнении команды</returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Delete([FromQuery] DeleteStoreHouseCommand deleteStoreHouseCommand)
    {
        var store = await _mediator.Send(deleteStoreHouseCommand);
        return Ok(store); 
    }
}