﻿using CQRS.Products.Commands.Models;
using CQRS.Products.Queries.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MyStoreHouse.Controllers;
/// <summary>
/// Контроллер запросов для товаров
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class ProductController : ControllerBase
{
    private readonly IMediator _mediator;
    /// <summary>
    /// Внедрение зависимости от проекта CQRS
    /// </summary>
    /// <param name="mediator"></param>
    public ProductController(IMediator mediator)
    {
        _mediator = mediator;
    }
    /// <summary>
    /// Получение продукта по его артикулу
    /// </summary>
    /// <param name="getProductByArticleQuery">Артикул продукта</param>
    /// <returns>Продукт из базы данных</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet("article")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(ProductDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Get([FromQuery]GetProductByArticleQuery getProductByArticleQuery)
    {
        var productDto = await _mediator.Send(getProductByArticleQuery);
        return Ok(productDto);
    }
    /// <summary>
    /// Получение всех продуктов
    /// </summary>
    /// <returns>Коллекция продуктов из базы данных</returns>
    [Authorize(Roles = "Guest, User, Admin")]
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(typeof(IEnumerable<ProductDto>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult<IEnumerable<ProductDto>>> GetAll()
    {
        var productsDto = await _mediator.Send(new GetAllProductsQuery());
        return Ok(productsDto);
    }
    /// <summary>
    /// Создание продукта
    /// </summary>
    /// <param name="createProductCommand">Новый продукт с заполненными полями</param>
    /// <returns>Идентификатор продукта</returns>
    [Authorize(Roles = "User, Admin")]
    [HttpPost]
    [Produces("application/json")]
    [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Create([FromBody] CreateProductCommand createProductCommand)
    {
        var productId = await _mediator.Send(createProductCommand);
        return Ok(productId);
    }
    /// <summary>
    /// Редактирование продукта
    /// </summary>
    /// <param name="editProductCommand">Продукт с заполненными полями для редактирования данных</param>
    /// <returns>Сообщение о выполнении команды</returns>
    [Authorize(Roles = "Admin")]
    [HttpPut]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Edit([FromBody] EditProductCommand editProductCommand)
    {
        var product = await _mediator.Send(editProductCommand);
        return Ok(product); 
    }
    /// <summary>
    /// Удаление продукта
    /// </summary>
    /// <param name="deleteProductCommand">Артикул удаляемого продукта</param>
    /// <returns>Сообщение о выполнении команды</returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BadRequestResult),StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResult),StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ForbidResult),StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Delete([FromQuery] DeleteProductCommand deleteProductCommand)
    {
        var product = await _mediator.Send(deleteProductCommand);
        return Ok(product); 
    }
}