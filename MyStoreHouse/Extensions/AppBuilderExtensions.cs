﻿using MyStoreHouse.Middlewares;

namespace MyStoreHouse.Extensions;

internal static class AppBuilderExtensions
{
    public static IApplicationBuilder UseCustomExceptionHandler(this IApplicationBuilder app)
    {
       return app.UseMiddleware<ExceptionsHandlerCustomMiddleware>();
    }

    public static IApplicationBuilder UsrLogUrl(this IApplicationBuilder app)
    {
        return app.UseMiddleware<LogUrlMiddleware>();
    }
}