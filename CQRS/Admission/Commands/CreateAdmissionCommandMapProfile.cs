﻿using AutoMapper;
using DataAccessLayer.DataModels;

namespace CQRS.Admission.Commands;
/// <summary>
/// Автомаппер CQRS модели для команды создания поступления
/// </summary>
public class CreateAdmissionCommandMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели создания поступления в таблицу поступлений в базе данных
    /// </summary>
    public CreateAdmissionCommandMapProfile()
    {
        CreateMap<CreateAdmissionCommand, AdmissionData>()
            .ForMember(admissionData => admissionData.AdmissionNumber,
                expression => expression.MapFrom(createAdmissionCommand => createAdmissionCommand.AdmissionNumber))
            .ForMember(admissionData => admissionData.StoreHouseId,
                expression => expression.MapFrom(createAdmissionCommand => createAdmissionCommand.StoreHouseId))
            .ForMember(admissionData => admissionData.Date,
                expression => expression.MapFrom(createAdmissionCommand => createAdmissionCommand.Date));
    }
}