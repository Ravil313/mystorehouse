﻿using AutoMapper;
using DataAccessLayer.DataModels;

namespace CQRS.Admission.Commands;
/// <summary>
/// Автомаппер CQRS модели для команды добавления товаров в поступление
/// </summary>
public class ProductsInAdmissionCommandMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели для команды добавления товаров в поступление
    /// </summary>
    public ProductsInAdmissionCommandMapProfile()
    {
        CreateMap<ProductsInAdmissionCommand, AdmissionProductsData>()
            .ForMember(admissionProductsData => admissionProductsData.ProductId,
                expression => expression.MapFrom(productsInAdmissionCommand => productsInAdmissionCommand.ProductId))
            .ForMember(admissionProductsData => admissionProductsData.Price,
                expression => expression.MapFrom(productsInAdmissionCommand => productsInAdmissionCommand.Price))
            .ForMember(admissionProductsData => admissionProductsData.Count,
                expression => expression.MapFrom(productsInAdmissionCommand => productsInAdmissionCommand.Count));
        CreateMap<ProductsInAdmissionCommand, ProductStoreHouseData>()
            .ForMember(productStoreHouseData => productStoreHouseData.ProductId,
                expression => expression.MapFrom(productsInAdmissionCommand => productsInAdmissionCommand.ProductId))
            .ForMember(productStoreHouseData => productStoreHouseData.Count,
                expression => expression.MapFrom(productsInAdmissionCommand => productsInAdmissionCommand.Count));
    }
}