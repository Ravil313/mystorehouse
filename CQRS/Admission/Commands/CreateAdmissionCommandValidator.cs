﻿using FluentValidation;

namespace CQRS.Admission.Commands;
/// <summary>
/// Валидатор CQRS модели для создания поступления
/// </summary>
public class CreateAdmissionCommandValidator : AbstractValidator<CreateAdmissionCommand>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для создания поступления
    /// </summary>
    public CreateAdmissionCommandValidator()
    {
        RuleFor(admissionCommand => admissionCommand.AdmissionNumber)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Номер поступления обязателен и должен быть >= 1");
        RuleFor(admissionCommand => admissionCommand.StoreHouseId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор склада обязателен и должен быть >= 1");
        RuleFor(admissionCommand => admissionCommand.Date)
            .NotEmpty()
            .NotNull()
            .MaximumLength(10)
            .WithMessage("Дата обязательна и не более 10 символов формата: 01.01.2020");
        RuleForEach(admissionCommand => admissionCommand.ProductsInAdmission)
            .SetValidator(new ProductsInAdmissionCommandValidator());
    }
}