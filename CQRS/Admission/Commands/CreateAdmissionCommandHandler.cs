﻿using AutoMapper;
using CQRS.CustomCheckers;
using CQRS.Exceptions;
using DataAccessLayer;
using DataAccessLayer.DataModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Admission.Commands;
/// <summary>
/// Обработчик CQRS команды создания поступления
/// </summary>
public class CreateAdmissionCommandHandler : IRequestHandler<CreateAdmissionCommand, int>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    private readonly ICustomChecker<CreateAdmissionCommand> _checker;
    /// <summary>
    /// Добавление зависимостей от базы данных, автомаппера и медиатора
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    /// <param name="checker"></param>
    public CreateAdmissionCommandHandler(DataContext dataContext, IMapper mapper, ICustomChecker<CreateAdmissionCommand> checker)
    {
        _dataContext = dataContext;
        _mapper = mapper;
        _checker = checker;
    }
    /// <summary>
    /// Метод обработчика CQRS команды создания поступления
    /// </summary>
    /// <param name="request">Запрос содержащий в теле заполненную CQRS модель для создания поступления</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    /// <exception cref="NotFoundException">Возникновение ошибки в случае не существования склада или товара необходимые для создания поступления</exception>
    public async Task<int> Handle(CreateAdmissionCommand request, CancellationToken cancellationToken)
    {
        await _checker.CommandRequestChecker(request, cancellationToken);
        var admissionData = _mapper.Map<AdmissionData>(request);
        await _dataContext.AdmissionsData.AddAsync(admissionData, cancellationToken : cancellationToken);
        await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        foreach (var productInAdmission in request.ProductsInAdmission!)
        {
            var admissionProductData = _mapper.Map<AdmissionProductsData>(productInAdmission);
            admissionProductData.AdmissionId = admissionData.AdmissionId;
            await _dataContext.AdmissionProductsData.AddAsync(admissionProductData, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        }
        await AddLinkInProductStoreHouseData(request, cancellationToken);
        await UpdateProductDataLastPurchasePrice(request, cancellationToken);
        return admissionData.AdmissionId;
    }
    /// <summary>
    /// Обновление последней закупочной цены продукта
    /// </summary>
    /// <param name="request">Запрос</param>
    /// <param name="cancellationToken"></param>
    private async Task UpdateProductDataLastPurchasePrice(CreateAdmissionCommand request, CancellationToken cancellationToken)
    {
        foreach (var productInAdmission in request.ProductsInAdmission!)
        {
            var productData = await _dataContext.ProductsData
                .FirstOrDefaultAsync(product => product.ProductId == productInAdmission.ProductId, cancellationToken);
            productData!.LastPurchasePrice = productInAdmission.Price;
            await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        }
    }
    /// <summary>
    /// Добавление связи в таблицу связей продукта и склада
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    private async Task AddLinkInProductStoreHouseData(CreateAdmissionCommand request, CancellationToken cancellationToken)
    {
        foreach (var productInAdmission in request.ProductsInAdmission!)
        {
            var productStoreHouseData = _mapper.Map<ProductStoreHouseData>(productInAdmission);
            productStoreHouseData.StoreHouseId = request.StoreHouseId;
            await _dataContext.ProductsStoreHousesData.AddAsync(productStoreHouseData, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        }
    }
}