﻿using FluentValidation;

namespace CQRS.Admission.Commands;
/// <summary>
/// Валидатор CQRS модели для добавления продуктов в поступление
/// </summary>
public class ProductsInAdmissionCommandValidator : AbstractValidator<ProductsInAdmissionCommand>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для добавления продуктов в поступление
    /// </summary>
    public ProductsInAdmissionCommandValidator()
    {
        RuleFor(productsInAdmission => productsInAdmission.ProductId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор товара обязателен и должен быть >= 1");
        RuleFor(productsInAdmission => productsInAdmission.Price)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Цена товара обязательна и должена быть >= 1");
        RuleFor(productsInAdmission => productsInAdmission.Count)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Количество товара обязателено и должено быть >= 1");
    }
}