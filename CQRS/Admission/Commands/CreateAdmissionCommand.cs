﻿using MediatR;

namespace CQRS.Admission.Commands;
/// <summary>
/// CQRS модель для создания поступления
/// </summary>
public class CreateAdmissionCommand : IRequest<int>
{
    /// <summary>
    /// Номер поступления
    /// </summary>
    public int AdmissionNumber { get; set; }
    /// <summary>
    /// Идентификатор склада для поступления
    /// </summary>
    public int StoreHouseId { get; set; }
    /// <summary>
    /// Дата поступления
    /// </summary>
    public string? Date { get; set; }
    /// <summary>
    /// Список товаров в поступлениии
    /// </summary>
    public ICollection<ProductsInAdmissionCommand>? ProductsInAdmission { get; set; }
}