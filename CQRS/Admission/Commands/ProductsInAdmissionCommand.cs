﻿namespace CQRS.Admission.Commands;
/// <summary>
/// Модель для создания связи товаров в поступлении
/// </summary>
public class ProductsInAdmissionCommand
{
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Закупочная цена товара
    /// </summary>
    public int Price { get; set; }
    /// <summary>
    /// Количество товара в поступлении
    /// </summary>
    public int Count { get; set; }
}