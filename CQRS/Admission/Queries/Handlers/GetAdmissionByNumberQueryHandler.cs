﻿using AutoMapper;
using CQRS.Admission.Queries.Models;
using CQRS.Exceptions;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Admission.Queries.Handlers;
/// <summary>
/// Обработчик CQRS запроса получения поступления по номеру
/// </summary>
public class GetAdmissionByNumberQueryHandler : IRequestHandler<GetAdmissionByNumberQuery, AdmissionDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetAdmissionByNumberQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS запроса получения поступления по номеру
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделю из строки запроса для получения поступления по номеру</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Модель поступления из базы данных</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если поступление не найдено</exception>
    public async Task<AdmissionDto> Handle(GetAdmissionByNumberQuery request, CancellationToken cancellationToken)
    {
        var admissionData = await _dataContext.AdmissionsData
            .AsNoTracking()
            .Include(admission => admission.AdmissionProducts)!
            .ThenInclude(admissionProductsData => admissionProductsData.ProductData)
            .FirstOrDefaultAsync(admissionData => admissionData.AdmissionNumber == request.AdmissionNumber, cancellationToken: cancellationToken);
        if (admissionData == null)
            throw new NotFoundException($"Поступление с номером: {request.AdmissionNumber} не найдено");
        return _mapper.Map<AdmissionDto>(admissionData);
    }
}