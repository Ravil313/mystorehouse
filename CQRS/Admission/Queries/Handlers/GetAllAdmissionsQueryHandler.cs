﻿using AutoMapper;
using CQRS.Admission.Queries.Models;
using CQRS.Exceptions;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Admission.Queries.Handlers;
/// <summary>
/// Обработчик CQRS запроса получения всех поступлений
/// </summary>
public class GetAllAdmissionsQueryHandler : IRequestHandler<GetAllAdmissionsQuery, IEnumerable<AdmissionDto>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetAllAdmissionsQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS запроса получения всех поступлений
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделю из строки запроса для получения всех поступлений</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Коллекция всех поступлений</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки список поступлений пуст</exception>
    public async Task<IEnumerable<AdmissionDto>> Handle(GetAllAdmissionsQuery request, CancellationToken cancellationToken)
    {
        var admissionsData = await _dataContext.AdmissionsData
            .AsNoTracking()
            .Include(admissionData => admissionData.AdmissionProducts)!
            .ThenInclude(admissionProductsData => admissionProductsData.ProductData)
            .ToListAsync(cancellationToken);
        if (admissionsData == null)
            throw new NotFoundException("Список поступлений пуст");
        return _mapper.Map<IEnumerable<AdmissionDto>>(admissionsData);
    }
}