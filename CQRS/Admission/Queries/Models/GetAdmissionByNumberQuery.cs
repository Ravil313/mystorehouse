﻿using MediatR;

namespace CQRS.Admission.Queries.Models;
/// <summary>
/// CQRS модель для получения поступления по номеру
/// </summary>
public class GetAdmissionByNumberQuery : IRequest<AdmissionDto>
{
    /// <summary>
    /// Номер поступления
    /// </summary>
    public int AdmissionNumber { get; set; } 
}