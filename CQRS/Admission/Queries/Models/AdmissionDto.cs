﻿namespace CQRS.Admission.Queries.Models;
/// <summary>
/// Модель для получения данных о постуалении товаров из базы данных
/// </summary>
public class AdmissionDto
{
    /// <summary>
    /// Идентификатор поступления
    /// </summary>
    public int AdmissionId { get; set; }
    /// <summary>
    /// Номер поступления
    /// </summary>
    public int AdmissionNumber { get; set; }
    /// <summary>
    /// Идентификатор склада
    /// </summary>
    public int StoreHouseId { get; set; }
    /// <summary>
    /// Дата поступления
    /// </summary>
    public string? Date { get; set; }
    /// <summary>
    /// Список товаров в поступлении
    /// </summary>
    public ICollection<ProductsInAdmissionDto>? ProductsInAdmission { get; set; }
}