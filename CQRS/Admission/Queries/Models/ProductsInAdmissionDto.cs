﻿namespace CQRS.Admission.Queries.Models;
/// <summary>
/// Модель для получения данных о товарах в таблице поступлении из базы данных
/// </summary>
public class ProductsInAdmissionDto
{
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Название товара
    /// </summary>
    public string? ProductName { get; set; }
    /// <summary>
    /// Закупочная цена товара
    /// </summary>
    public int Price { get; set; }
    /// <summary>
    /// Количество товара в поступлении
    /// </summary>
    public int Count { get; set; }
}