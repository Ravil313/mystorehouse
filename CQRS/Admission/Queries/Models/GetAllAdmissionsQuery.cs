﻿using MediatR;

namespace CQRS.Admission.Queries.Models;
/// <summary>
/// CQRS Модель для поучения всех поступлений
/// </summary>
public class GetAllAdmissionsQuery : IRequest<IEnumerable<AdmissionDto>>
{ }