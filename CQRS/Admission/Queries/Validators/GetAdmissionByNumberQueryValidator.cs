﻿using CQRS.Admission.Queries.Models;
using FluentValidation;

namespace CQRS.Admission.Queries.Validators;
/// <summary>
/// Валидатор CQRS модели для получения поступления по номеру 
/// </summary>
public class GetAdmissionByNumberQueryValidator : AbstractValidator<GetAdmissionByNumberQuery>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для получения поступления по номеру 
    /// </summary>
    public GetAdmissionByNumberQueryValidator()
    {
        RuleFor(adm => adm.AdmissionNumber)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Номер поступления обязателен и должен быть >= 1");
    }
}