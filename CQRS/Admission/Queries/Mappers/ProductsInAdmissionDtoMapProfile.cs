﻿using AutoMapper;
using CQRS.Admission.Queries.Models;
using DataAccessLayer.DataModels;

namespace CQRS.Admission.Queries.Mappers;
/// <summary>
/// Автомаппер CQRS модели для запроса получения продукта в составе поступления с моделью продуктов в базе дыных из таблицы связей поступления и продуктов
/// </summary>
public class ProductsInAdmissionDtoMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели для запроса получения продукта в составе поступления с моделью продуктов в базе дыных из таблицы связей поступления и продуктов
    /// </summary>
    public ProductsInAdmissionDtoMapProfile()
    {
        CreateMap<AdmissionProductsData, ProductsInAdmissionDto>()
            .ForMember(productsInAdmission => productsInAdmission.ProductId,expression => expression
                .MapFrom(admissionProductsData => admissionProductsData.ProductId))
            .ForMember(productsInAdmission => productsInAdmission.ProductName,expression => expression
                .MapFrom(admissionProductsData => admissionProductsData.ProductData!.ProductDataName))
            .ForMember(productsInAdmission => productsInAdmission.Price,expression => expression
                .MapFrom(admissionProductsData => admissionProductsData.Price))
            .ForMember(productsInAdmission => productsInAdmission.Count,expression => expression
                .MapFrom(admissionProductsData => admissionProductsData.Count));
    }
}