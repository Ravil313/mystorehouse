﻿using AutoMapper;
using CQRS.Admission.Queries.Models;
using CQRS.Products.Queries.Models;
using DataAccessLayer.DataModels;

namespace CQRS.Admission.Queries.Mappers;
/// <summary>
/// Автомаппер модели CQRS запроса для получения поступления из таблицы поступлений в базе дынных
/// </summary>
public class AdmissionDtoMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга модели CQRS запроса для получения поступления из таблицы поступлений в базе дынных
    /// </summary>
    public AdmissionDtoMapProfile()
    {
        CreateMap<AdmissionData, AdmissionDto>()
            .ForMember(admissionDto => admissionDto.AdmissionId,expression => expression.MapFrom(admissionData => admissionData.AdmissionId))
            .ForMember(admissionDto => admissionDto.AdmissionNumber,expression => expression.MapFrom(admissionData => admissionData.AdmissionNumber))
            .ForMember(admissionDto => admissionDto.StoreHouseId,expression => expression.MapFrom(admissionData => admissionData.StoreHouseId))
            .ForMember(admissionDto => admissionDto.Date,expression => expression.MapFrom(admissionData => admissionData.Date))
            .ForMember(admissionDto => admissionDto.ProductsInAdmission,expression => expression.MapFrom(admissionData => admissionData.AdmissionProducts));
    }
}