﻿using CQRS.Admission.Commands;
using MediatR;

namespace CQRS.Moving.Commands;
/// <summary>
/// CQRS модель для создания перемещения
/// </summary>
public class CreateMovingCommand : IRequest<int>
{
    /// <summary>
    /// Номер перемещения
    /// </summary>
    public int MovingNumber { get; set; }
    /// <summary>
    /// Идентификатор склада из которого перемещаются товары
    /// </summary>
    public int StoreHouseIdFrom { get; set; }
    /// <summary>
    /// Идентификатор склада в который перемещаютя товары
    /// </summary>
    public int StoreHouseIdTo { get; set; }
    /// <summary>
    /// Дата перемещения
    /// </summary>
    public string? Date { get; set; }
    /// <summary>
    /// Список товаров в перемещении
    /// </summary>
    public ICollection<ProductsInMovingCommand>? ProductsInMoving { get; set; }
}