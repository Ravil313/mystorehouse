﻿using AutoMapper;
using CQRS.CustomCheckers;
using CQRS.Exceptions;
using DataAccessLayer;
using DataAccessLayer.DataModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Moving.Commands;
/// <summary>
/// Обработчик CQRS команды создания поступления
/// </summary>
public class CreateMovingCommandHandler : IRequestHandler<CreateMovingCommand, int>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    private readonly ICustomChecker<CreateMovingCommand> _checker;
    /// <summary>
    /// Добавление зависимостей от базы данных, автомаппера и медиатора
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    /// <param name="checker"></param>
    public CreateMovingCommandHandler(DataContext dataContext, IMapper mapper, ICustomChecker<CreateMovingCommand> checker)
    {
        _dataContext = dataContext;
        _mapper = mapper;
        _checker = checker;
    }
    /// <summary>
    /// Метод обработчика CQRS команды создания поступления
    /// </summary>
    /// <param name="request">Запрос содержащий в теле заполненную CQRS модель для создания поступления</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    /// <exception cref="NotFoundException">Возникновение ошибки в случае не существования склада или товара необходимые для создания поступления</exception>
    public async Task<int> Handle(CreateMovingCommand request, CancellationToken cancellationToken)
    {
        await _checker.CommandRequestChecker(request, cancellationToken);
        try
        {
            await EdinLinkInProductStoreHouseData(request, cancellationToken);
        }
        catch (ArgumentOutOfRangeException e)
        {
            Console.WriteLine(e);
            throw;
        }
        var movingData = _mapper.Map<MovingData>(request);
        await _dataContext.MovingData.AddAsync(movingData, cancellationToken : cancellationToken);
        await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        foreach (var productInMoving in request.ProductsInMoving!)
        {
            var movingProductData = _mapper.Map<MovingProductsData>(productInMoving);
            movingProductData.MovingId = movingData.MovingId;
            await _dataContext.MovingProductsData.AddAsync(movingProductData, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        }
        await CreateNewOrEditLinkInProductStoreHouseData(request, cancellationToken);
        return movingData.MovingId;
    }
    /// <summary>
    /// Создание новой или редактирование связи при перемещении товара в таблице связей продукта и склада
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    private async Task CreateNewOrEditLinkInProductStoreHouseData(CreateMovingCommand request, CancellationToken cancellationToken)
    {
        foreach (var productInMoving in request.ProductsInMoving!)
        {
           
            var productStoreHouseTo = await _dataContext.ProductsStoreHousesData
                .FirstOrDefaultAsync(productStore => productStore.StoreHouseId == request.StoreHouseIdTo && productStore.ProductId == productInMoving.ProductId, cancellationToken);
           if (productStoreHouseTo == null)
            {
                var newProductStoreHouseData = _mapper.Map<ProductStoreHouseData>(productInMoving);
                newProductStoreHouseData.StoreHouseId = request.StoreHouseIdTo;
                await _dataContext.ProductsStoreHousesData.AddAsync(newProductStoreHouseData, cancellationToken);
                await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
            }
            else
            {
                _dataContext.ProductsStoreHousesData.Remove(productStoreHouseTo);
                await _dataContext.SaveChangesAsync(cancellationToken: cancellationToken);
                productStoreHouseTo.Count += productInMoving.Count;
                await _dataContext.ProductsStoreHousesData.AddAsync(productStoreHouseTo, cancellationToken);
                await _dataContext.SaveChangesAsync(cancellationToken: cancellationToken);
            }
        }
    }
    /// <summary>
    /// Редактирование связи из которой забирается товар в таблице связей продукта и склада
    /// </summary>
    /// <param name="request">запрос</param>
    /// <param name="cancellationToken"></param>
    /// <exception cref="ArgumentOutOfRangeException">Возвращение ошибки при попытке переместить больше количества товара, чем есть на складе</exception>
    private async Task EdinLinkInProductStoreHouseData(CreateMovingCommand request, CancellationToken cancellationToken)
    {
        foreach (var productInMoving in request.ProductsInMoving!)
        {
            var productStoreHouseFrom = await _dataContext.ProductsStoreHousesData
                .FirstOrDefaultAsync(productStore => productStore.StoreHouseId == request.StoreHouseIdFrom && productStore.ProductId == productInMoving.ProductId, cancellationToken);
            if (productStoreHouseFrom!.Count < productInMoving.Count)
                throw new ArgumentOutOfRangeException(nameof(productInMoving.Count), $"Количество перемещаемого товара больше, чем есть на складе {request.StoreHouseIdFrom}");
            if (productStoreHouseFrom.Count == productInMoving.Count)
            {
                _dataContext.ProductsStoreHousesData.Remove(productStoreHouseFrom);
                await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
            }
            else
            {
                _dataContext.ProductsStoreHousesData.Remove(productStoreHouseFrom);
                await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
                productStoreHouseFrom.Count -= productInMoving.Count;
                await _dataContext.ProductsStoreHousesData.AddAsync(productStoreHouseFrom, cancellationToken);
                await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
            }
        }
    }
}