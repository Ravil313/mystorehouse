﻿namespace CQRS.Moving.Commands;
/// <summary>
/// Модель для создания связи товаров в перемещении
/// </summary>
public class ProductsInMovingCommand
{
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Количество товара в перемещении
    /// </summary>
    public int Count { get; set; }
}