﻿using FluentValidation;

namespace CQRS.Moving.Commands;
/// <summary>
/// Валидатор CQRS модели для создания перемещения
/// </summary>
public class CreateMovingCommandValidator : AbstractValidator<CreateMovingCommand>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для создания перемещения
    /// </summary>
    public CreateMovingCommandValidator()
    {
        RuleFor(movingCommand => movingCommand.MovingNumber)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Номер перемещения обязателен и должен быть >= 1");
        RuleFor(movingCommand => movingCommand.StoreHouseIdFrom)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор из перемещаемого склада обязателен и должен быть >= 1");
        RuleFor(movingCommand => movingCommand.StoreHouseIdTo)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор в перемещаемый склад обязателен и должен быть >= 1");
        RuleFor(movingCommand => movingCommand.Date)
            .NotEmpty()
            .NotNull()
            .MaximumLength(10)
            .WithMessage("Дата обязательна и не более 10 символов формата: 01.01.2020");
        RuleForEach(movingCommand => movingCommand.ProductsInMoving)
            .SetValidator(new ProductsInMovingCommandValidator());
    }
}