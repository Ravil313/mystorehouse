﻿using FluentValidation;

namespace CQRS.Moving.Commands;
/// <summary>
/// Валидатор CQRS модели для добавления продуктов в перемещение
/// </summary>
public class ProductsInMovingCommandValidator : AbstractValidator<ProductsInMovingCommand>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для добавления продуктов в перемещение
    /// </summary>
    public ProductsInMovingCommandValidator()
    {
        RuleFor(inMovingCommand => inMovingCommand.ProductId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор товара обязателен и должен быть >= 1");
        RuleFor(inMovingCommand => inMovingCommand.Count)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Количество товара обязателено и должено быть >= 1");
    }
}