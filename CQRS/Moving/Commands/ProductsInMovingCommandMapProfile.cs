﻿using AutoMapper;
using DataAccessLayer.DataModels;

namespace CQRS.Moving.Commands;
/// <summary>
/// Автомаппер CQRS модели для команды добавления товаров в перемещении
/// </summary>
public class ProductsInMovingCommandMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели для команды добавления товаров в перемещении
    /// </summary>
    public ProductsInMovingCommandMapProfile()
    {
        CreateMap<ProductsInMovingCommand, MovingProductsData>()
            .ForMember(movingProductsData => movingProductsData.ProductId,
                expression => expression.MapFrom(productsInMovingCommand => productsInMovingCommand.ProductId))
            .ForMember(movingProductsData => movingProductsData.Count,
                expression => expression.MapFrom(productsInMovingCommand => productsInMovingCommand.Count));
        CreateMap<ProductsInMovingCommand, ProductStoreHouseData>()
            .ForMember(productStoreHouseData => productStoreHouseData.ProductId,
                expression => expression.MapFrom(productsInMovingCommand => productsInMovingCommand.ProductId))
            .ForMember(productStoreHouseData => productStoreHouseData.Count,
                expression => expression.MapFrom(productsInMovingCommand => productsInMovingCommand.Count));
    }
}