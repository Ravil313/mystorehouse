﻿using AutoMapper;
using DataAccessLayer.DataModels;

namespace CQRS.Moving.Commands;
/// <summary>
/// Автомаппер CQRS модели для команды создания перемещения
/// </summary>
public class CreateMovingCommandMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели создания перемещения в таблицу перемещений в базе данных
    /// </summary>
    public CreateMovingCommandMapProfile()
    {
        CreateMap<CreateMovingCommand, MovingData>()
            .ForMember(movingData => movingData.MovingNumber,
                expression => expression.MapFrom(createMovingCommand => createMovingCommand.MovingNumber))
            .ForMember(movingData => movingData.StoreHouseIdFrom,
                expression => expression.MapFrom(createMovingCommand => createMovingCommand.StoreHouseIdFrom))
            .ForMember(movingData => movingData.StoreHouseIdTo,
                expression => expression.MapFrom(createMovingCommand => createMovingCommand.StoreHouseIdTo))
            .ForMember(movingData => movingData.Date,
                expression => expression.MapFrom(createMovingCommand => createMovingCommand.Date));
    }
}