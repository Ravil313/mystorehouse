﻿using AutoMapper;
using CQRS.Admission.Queries.Models;
using CQRS.Exceptions;
using CQRS.Moving.Queries.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Moving.Queries.Handlers;
/// <summary>
/// Обработчик CQRS запроса получения всех перемещений
/// </summary>
public class GetAllMovingQueryHandler : IRequestHandler<GetAllMovingQuery, IEnumerable<MovingDto>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetAllMovingQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS запроса получения всех перемещений
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделю из строки запроса для получения всех перемещений</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Коллекция всех перемещений</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки список перемещений пуст</exception>
    public async Task<IEnumerable<MovingDto>> Handle(GetAllMovingQuery request, CancellationToken cancellationToken)
    {
        var movingData = await _dataContext.MovingData
            .AsNoTracking()
            .Include(moving => moving.MovingProducts)!
            .ThenInclude(movingProductsData => movingProductsData.ProductData)
            .ToListAsync(cancellationToken);
        if (movingData == null)
            throw new NotFoundException("Список перемещений пуст");
        return _mapper.Map<IEnumerable<MovingDto>>(movingData);
    }
}