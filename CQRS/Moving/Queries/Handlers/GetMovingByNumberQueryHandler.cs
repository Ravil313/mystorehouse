﻿using AutoMapper;
using CQRS.Exceptions;
using CQRS.Moving.Queries.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Moving.Queries.Handlers;
/// <summary>
/// Обработчик CQRS запроса получения перемещения по номеру
/// </summary>
public class GetMovingByNumberQueryHandler : IRequestHandler<GetMovingByNumberQuery, MovingDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetMovingByNumberQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS запроса получения перемещения по номеру
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделю из строки запроса для получения перемещения по номеру</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Модель перемещения из базы данных</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если перемещение не найдено</exception>
    public async Task<MovingDto> Handle(GetMovingByNumberQuery request, CancellationToken cancellationToken)
    {
        var movingData = await _dataContext.MovingData
            .AsNoTracking()
            .Include(moving => moving.MovingProducts)!
            .ThenInclude(productsData => productsData.ProductData)
            .FirstOrDefaultAsync(movingData => movingData.MovingNumber == request.MovingNumber, cancellationToken: cancellationToken);
        if (movingData == null)
            throw new NotFoundException($"Перемещение с номером: {request.MovingNumber} не найдено");
        return _mapper.Map<MovingDto>(movingData);
    }
}