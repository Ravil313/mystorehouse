﻿using AutoMapper;
using CQRS.Moving.Queries.Models;
using DataAccessLayer.DataModels;

namespace CQRS.Moving.Queries.Mappers;
/// <summary>
/// Автомаппер модели CQRS запроса для получения перемещения из таблицы перемещений в базе дынных
/// </summary>
public class MovingDtoMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга модели CQRS запроса для получения перемещения из таблицы перемещений в базе дынных
    /// </summary>
    public MovingDtoMapProfile()
    {
        CreateMap<MovingData, MovingDto>()
            .ForMember(movingDto => movingDto.MovingId,expression => expression.MapFrom(movingData => movingData.MovingId))
            .ForMember(movingDto => movingDto.MovingNumber,expression => expression.MapFrom(movingData => movingData.MovingNumber))
            .ForMember(movingDto => movingDto.StoreHouseIdFrom,expression => expression.MapFrom(movingData => movingData.StoreHouseIdFrom))
            .ForMember(movingDto => movingDto.StoreHouseIdTo,expression => expression.MapFrom(movingData => movingData.StoreHouseIdTo))
            .ForMember(movingDto => movingDto.Date,expression => expression.MapFrom(movingData => movingData.Date))
            .ForMember(movingDto => movingDto.ProductsInMoving,expression => expression.MapFrom(movingData => movingData.MovingProducts));
    }
}