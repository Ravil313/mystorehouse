﻿using AutoMapper;
using CQRS.Moving.Queries.Models;
using DataAccessLayer.DataModels;

namespace CQRS.Moving.Queries.Mappers;
/// <summary>
/// Автомаппер CQRS модели для запроса получения продукта в составе перемещения с моделью продуктов в базе дыных из таблицы связей перемещений и продуктов
/// </summary>
public class ProductsInMovingMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели для запроса получения продукта в составе перемещения с моделью продуктов в базе дыных из таблицы связей перемещений и продуктов
    /// </summary>
    public ProductsInMovingMapProfile()
    {
        CreateMap<MovingProductsData, ProductsInMovingDto>()
            .ForMember(productsInMoving => productsInMoving.ProductId,expression => expression
                .MapFrom(movingProductsData => movingProductsData.ProductId))
            .ForMember(productsInMoving => productsInMoving.ProductName,expression => expression
                .MapFrom(movingProductsData => movingProductsData.ProductData!.ProductDataName))
            .ForMember(productsInAdmission => productsInAdmission.Count,expression => expression
                .MapFrom(admissionProductsData => admissionProductsData.Count));
    }
}