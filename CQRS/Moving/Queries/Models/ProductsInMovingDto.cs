﻿namespace CQRS.Moving.Queries.Models;
/// <summary>
/// Модель для получения данных о товарах в таблице перемещении из базы данных
/// </summary>
public class ProductsInMovingDto
{
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Название товара
    /// </summary>
    public string? ProductName { get; set; }
    /// <summary>
    /// Количество товара в перемещении
    /// </summary>
    public int Count { get; set; }
}