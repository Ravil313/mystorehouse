﻿namespace CQRS.Moving.Queries.Models;
/// <summary>
/// Модель для получения данных о передвижении товаров из базы данных
/// </summary>
public class MovingDto
{
    /// <summary>
    /// Идентификатор передвижения
    /// </summary>
    public int MovingId { get; set; }
    /// <summary>
    /// Номер передвижения
    /// </summary>
    public int MovingNumber { get; set; }
    /// <summary>
    /// Идентификатор склада из которого перемещается товар
    /// </summary>
    public int StoreHouseIdFrom { get; set; }
    /// <summary>
    /// Идентификатор склада в который перемещается товар
    /// </summary>
    public int StoreHouseIdTo { get; set; }
    /// <summary>
    /// Дата перемещения
    /// </summary>
    public string? Date { get; set; }
    /// <summary>
    /// Список товаров в перемещении
    /// </summary>
    public ICollection<ProductsInMovingDto>? ProductsInMoving { get; set; }
}