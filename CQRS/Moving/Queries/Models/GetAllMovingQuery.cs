﻿using MediatR;

namespace CQRS.Moving.Queries.Models;
/// <summary>
/// CQRS Модель для поучения всех перемещений
/// </summary>
public class GetAllMovingQuery : IRequest<IEnumerable<MovingDto>>
{ }