﻿using MediatR;

namespace CQRS.Moving.Queries.Models;
/// <summary>
/// CQRS модель для получения перемещения по номеру
/// </summary>
public class GetMovingByNumberQuery : IRequest<MovingDto>
{
    /// <summary>
    /// Номер перемещения
    /// </summary>
    public int MovingNumber { get; set; } 
}