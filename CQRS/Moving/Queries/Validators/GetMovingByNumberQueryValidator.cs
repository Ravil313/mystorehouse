﻿using CQRS.Moving.Queries.Models;
using FluentValidation;

namespace CQRS.Moving.Queries.Validators;
/// <summary>
/// Валидатор CQRS модели для получения перемещения по номеру 
/// </summary>
public class GetMovingByNumberQueryValidator : AbstractValidator<GetMovingByNumberQuery>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для получения перемещения по номеру 
    /// </summary>
    public GetMovingByNumberQueryValidator()
    {
        RuleFor(moving => moving.MovingNumber)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Номер перемещения обязателен и должен быть >= 1");
    }
}