﻿namespace CQRS.Security;

/// <summary>
/// 
/// </summary>
public interface IJWTProvider
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="login"></param>
    /// <param name="role"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    string GetToken(string login, string role, int id);
}