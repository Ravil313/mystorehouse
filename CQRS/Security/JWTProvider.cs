﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace CQRS.Security;

/// <summary>
/// Класс для создания JWT токена
/// </summary>
public class JWTProvider : IJWTProvider
{
    /// <summary>
    /// создание JWT токена
    /// </summary>
    /// <param name="login">Логин</param>
    /// <param name="role">Роль</param>
    /// <param name="id">Идентификатор</param>
    /// <returns></returns>
    public string GetToken(string login, string role, int id)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var keyval = "my-secret-key-my-secret-key-my-secret-key";
        var key = Encoding.ASCII.GetBytes(keyval);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, role),
                new Claim(ClaimTypes.Sid, id.ToString())
            }),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature),
        
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var res = tokenHandler.WriteToken(token);
        return res;
    }
}