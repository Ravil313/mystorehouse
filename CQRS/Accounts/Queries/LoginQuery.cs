﻿using MediatR;

namespace CQRS.Accounts.Queries;

/// <summary>
/// Модель для ввода Логина и Пароля
/// </summary>
public class LoginQuery : IRequest<string>
{
    /// <summary>
    /// Логин
    /// </summary>
    public string Login { get; set; }
    /// <summary>
    /// Пароль
    /// </summary>
    public string Password { get; set; }
}