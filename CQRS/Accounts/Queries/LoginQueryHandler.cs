﻿using CQRS.Security;
using DataAccessLayer;
using MediatR;

namespace CQRS.Accounts.Queries;

/// <summary>
/// Обработчик запроса на получение JWT токена
/// </summary>
public class LoginQueryHandler : IRequestHandler<LoginQuery, string>
{
    private readonly DataContext _dataContext;
    private readonly IJWTProvider _jwtTokenProvider;

    /// <summary>
    /// Внедрение зависимостей
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="jwtTokenProvider"></param>
    public LoginQueryHandler(DataContext dataContext, IJWTProvider jwtTokenProvider)
    {
        _dataContext = dataContext;
        _jwtTokenProvider = jwtTokenProvider;
    }

    /// <summary>
    /// Возвращает JWT токен для пользователя
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task<string> Handle(LoginQuery request, CancellationToken cancellationToken)
    {
        var user = _dataContext.UsersData.First(user1 => user1.Login == request.Login && user1.Password == request.Password);
        return Task.FromResult(_jwtTokenProvider.GetToken(user.Login!, user.Role.ToString(), user.Id));
    }
}