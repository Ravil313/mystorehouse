﻿namespace CQRS.CustomCheckers;

/// <summary>
/// Интерфейс проверок для документов
/// </summary>
public interface ICustomChecker<in T>
{
   /// <summary>
   /// Проверка запроса на существование необходимых записей в таблицах БД
   /// </summary>
   /// <param name="command">Модель команды</param>
   /// <param name="cancellationToken">cancellationToken</param>
   /// <returns>Task</returns>
   public Task CommandRequestChecker(T command, CancellationToken cancellationToken);
}