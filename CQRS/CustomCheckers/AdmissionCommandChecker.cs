﻿using CQRS.Admission.Commands;
using CQRS.Exceptions;
using DataAccessLayer;
using Microsoft.EntityFrameworkCore;

namespace CQRS.CustomCheckers;

/// <summary>
/// Проверки перед созданием поступления
/// </summary>
public class AdmissionCommandChecker : ICustomChecker<CreateAdmissionCommand>
{
    private readonly DataContext _dataContext;
    /// <summary>
    /// Внедрение зависимости от БД
    /// </summary>
    /// <param name="dataContext"></param>
    public AdmissionCommandChecker(DataContext dataContext)
    {
        _dataContext = dataContext;
    }
    /// <summary>
    /// Проверка команды создания поступления на существование необходимых записей с таблицах БД
    /// </summary>
    /// <param name="command">Модель создания перемещения</param>
    /// <param name="cancellationToken">cancellationToken</param>
    public async Task CommandRequestChecker(CreateAdmissionCommand command, CancellationToken cancellationToken)
    {
       await DocumentExistenceCheck(command.AdmissionNumber, cancellationToken);
       await ProductExistenceCheck(command.ProductsInAdmission!, cancellationToken);
       await StoreHouseExistenceCheck(command.StoreHouseId, cancellationToken);
    }
    private async Task DocumentExistenceCheck(int id, CancellationToken cancellationToken)
    {
        var movingData = await _dataContext.AdmissionsData
            .AsNoTracking()
            .FirstOrDefaultAsync(admissionData => admissionData.AdmissionNumber == id, cancellationToken);
        if (movingData != null)
            throw new SameObjectException($"Поступление с номером {id} уже существует, введите другой номер!");
    }
    private async Task ProductExistenceCheck(ICollection<ProductsInAdmissionCommand> productsInMoving, CancellationToken cancellationToken)
    {
        if (productsInMoving == null)
            throw new ArgumentNullException(nameof(productsInMoving));

        foreach (var productInMoving in productsInMoving)
        {
            var productData = await _dataContext.ProductsData
                .AsNoTracking()
                .FirstOrDefaultAsync(productData => productData.ProductId == productInMoving.ProductId, cancellationToken);
            if (productData == null)
                throw new NotFoundException($"Товар с идентификатором {productInMoving.ProductId} не существует, создайте данный товар!");
        }
    }
    private async Task StoreHouseExistenceCheck(int id, CancellationToken cancellationToken)
    {
        var storeHouseData = await _dataContext.StoreHousesData
            .AsNoTracking()
            .FirstOrDefaultAsync(storeHouseData => storeHouseData.StoreHouseId == id, cancellationToken);
        if(storeHouseData == null)
            throw new NotFoundException($"Склад c идентификатором {id} не существует, создайте данный склад!");
    }
}