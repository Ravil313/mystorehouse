﻿using CQRS.Exceptions;
using CQRS.Sell.Commands;
using DataAccessLayer;
using Microsoft.EntityFrameworkCore;

namespace CQRS.CustomCheckers;

/// <summary>
/// Проверки перед созданием продажи
/// </summary>
public class SellCommandChecker : ICustomChecker<CreateSellCommand>
{
    private readonly DataContext _dataContext;
    /// <summary>
    /// Внедрение зависимости от БД
    /// </summary>
    /// <param name="dataContext"></param>
    public SellCommandChecker(DataContext dataContext)
    {
        _dataContext = dataContext;
    }
    /// <summary>
    /// Проверка команды создания продажи на существование необходимых записей с таблицах БД
    /// </summary>
    /// <param name="command">Модель создания перемещения</param>
    /// <param name="cancellationToken">cancellationToken</param>
    public async Task CommandRequestChecker(CreateSellCommand command, CancellationToken cancellationToken)
    {
       await DocumentExistenceCheck(command.SellNumber, cancellationToken);
       await ProductExistenceCheck(command.ProductsInSell!, cancellationToken);
       await StoreHouseExistenceCheck(command.StoreHouseId, cancellationToken);
    }
    private async Task DocumentExistenceCheck(int id, CancellationToken cancellationToken)
    {
        var movingData = await _dataContext.SellsData
            .AsNoTracking()
            .FirstOrDefaultAsync(sellData => sellData.SellNumber == id, cancellationToken);
        if (movingData != null)
            throw new SameObjectException($"Продажа с номером {id} уже существует, введите другой номер!");
    }
    private async Task ProductExistenceCheck(ICollection<ProductsInSellCommand> productsInMoving, CancellationToken cancellationToken)
    {
        if (productsInMoving == null)
            throw new ArgumentNullException(nameof(productsInMoving));

        foreach (var productInMoving in productsInMoving)
        {
            var productData = await _dataContext.ProductsData
                .AsNoTracking()
                .FirstOrDefaultAsync(productData => productData.ProductId == productInMoving.ProductId, cancellationToken);
            if (productData == null)
                throw new NotFoundException($"Товар с идентификатором {productInMoving.ProductId} не существует, создайте данный товар!");
        }
    }
    private async Task StoreHouseExistenceCheck(int id, CancellationToken cancellationToken)
    {
        var storeHouseData = await _dataContext.StoreHousesData
            .AsNoTracking()
            .FirstOrDefaultAsync(storeHouseData => storeHouseData.StoreHouseId == id, cancellationToken);
        if(storeHouseData == null)
            throw new NotFoundException($"Склад c идентификатором {id} не существует, создайте данный склад!");
    }
}