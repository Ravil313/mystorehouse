﻿using CQRS.ProductStoreHouseLinks.Commands.Models;
using FluentValidation;

namespace CQRS.ProductStoreHouseLinks.Commands.Validators;
/// <summary>
/// Валидатор CQRS модели для редактирования связи товара и склада
/// </summary>
public class EditProductStoreHouseLinkValidator : AbstractValidator<EditProductStoreHouseLink>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для редактирования связи товара и склада
    /// </summary>
    public EditProductStoreHouseLinkValidator()
    {
        RuleFor(pro => pro.FromProductId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор товара обязателен и должен быть >= 1");
        RuleFor(pro => pro.FromStoreHouseId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор склада обязателен и должен быть >= 1");
        RuleFor(pro => pro.ToProductId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Новый идентификатор товара обязателен и должен быть >= 1");
        RuleFor(pro => pro.ToStoreHouseId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Новый идентификатор склада обязателен и должен быть >= 1");
        RuleFor(pro => pro.NewCount)
            .GreaterThanOrEqualTo(0)
            .WithMessage("Новое количество товара должно быть >= 0");
    }
}