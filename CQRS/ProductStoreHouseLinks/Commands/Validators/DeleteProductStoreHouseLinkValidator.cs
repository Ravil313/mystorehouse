﻿using CQRS.ProductStoreHouseLinks.Commands.Models;
using FluentValidation;

namespace CQRS.ProductStoreHouseLinks.Commands.Validators;
/// <summary>
/// Валидатор CQRS модели для удаления связи товара и склада
/// </summary>
public class DeleteProductStoreHouseLinkValidator : AbstractValidator<DeleteProductStoreHouseLink>
{
    /// <summary>
    /// Правила валидации полей модели для удаления связи товара и склада
    /// </summary>
    public DeleteProductStoreHouseLinkValidator()
    {
        RuleFor(pro => pro.ProductId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор товара обязателен и должен быть >= 1");
        RuleFor(pro => pro.StoreHouseId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор склада обязателен и должен быть >= 1");
    }
}