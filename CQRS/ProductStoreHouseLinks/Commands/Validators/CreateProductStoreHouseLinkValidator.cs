﻿using CQRS.ProductStoreHouseLinks.Commands.Models;
using FluentValidation;

namespace CQRS.ProductStoreHouseLinks.Commands.Validators;
/// <summary>
/// Валидатор CQRS модели для создания связи товара и склада
/// </summary>
public class CreateProductStoreHouseLinkValidator : AbstractValidator<CreateProductStoreHouseLink>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для создания связи товара и склада
    /// </summary>
    public CreateProductStoreHouseLinkValidator()
    {
        RuleFor(pro => pro.ProductId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор товара обязателен и должен быть >= 1");
        RuleFor(pro => pro.StoreHouseId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор склада обязателен и должен быть >= 1");
        RuleFor(pro => pro.Count)
            .GreaterThanOrEqualTo(0)
            .WithMessage("Количество товара должно быть >= 0");
    }
}