﻿using AutoMapper;
using CQRS.ProductStoreHouseLinks.Commands.Models;
using DataAccessLayer;
using DataAccessLayer.DataModels;
using MediatR;

namespace CQRS.ProductStoreHouseLinks.Commands.Handlers;
/// <summary>
/// Обработчик CQRS команды создания связи товара и склада
/// </summary>
public class CreateProductStoreHouseLinkHandler : IRequestHandler<CreateProductStoreHouseLink, string>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public CreateProductStoreHouseLinkHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS команды создания связи товара и склада
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделью из тела запроса для создания связи</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Сообщение о успешном добавлении связи</returns>
    public async Task<string> Handle(CreateProductStoreHouseLink request, CancellationToken cancellationToken)
    {
        var productStoreHouseLink = _mapper.Map<ProductStoreHouseData>(request);
        await _dataContext.ProductsStoreHousesData.AddAsync(productStoreHouseLink, cancellationToken : cancellationToken);
        await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        return "Связь добавлена";
    }
}