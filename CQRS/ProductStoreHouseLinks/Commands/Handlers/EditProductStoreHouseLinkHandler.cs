﻿using AutoMapper;
using CQRS.Exceptions;
using CQRS.Products.Commands.Models;
using CQRS.ProductStoreHouseLinks.Commands.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.ProductStoreHouseLinks.Commands.Handlers;
/// <summary>
/// Обработчик CQRS команды редактирования свзяи товара и склада
/// </summary>
public class EditProductStoreHouseLinkHandler : IRequestHandler<EditProductStoreHouseLink, string>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public EditProductStoreHouseLinkHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS команды редактирования свзяи товара и склада
    /// </summary>
    /// <param name="request">Запрос с заполненной моделью из тела запроса для редактирования свзяи</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Сообщение о успешном редактировании связи</returns>
    /// <exception cref="SameObjectException">Возвращение ошибки если связь уже существует</exception>
    /// <exception cref="NotFoundException">Возвращение ошибки если связь не найдена</exception>
    public async Task<string> Handle(EditProductStoreHouseLink request, CancellationToken cancellationToken)
    {
        var productStoreHouseLink = await _dataContext.ProductsStoreHousesData
            .FirstOrDefaultAsync(psh => psh.ProductId == request.FromProductId && psh.StoreHouseId == request.FromStoreHouseId, cancellationToken: cancellationToken);
        var newProductStoreHouseLinkInDb = await _dataContext.ProductsStoreHousesData
            .FirstOrDefaultAsync(psh => psh.ProductId == request.ToProductId && psh.StoreHouseId == request.ToStoreHouseId, cancellationToken: cancellationToken);
        if (newProductStoreHouseLinkInDb != null)
            throw new SameObjectException($"Связь {request.ToProductId} с {request.ToStoreHouseId} уже существует");
        if (productStoreHouseLink == null)
            throw new NotFoundException($"Связь {request.FromProductId} с {request.FromStoreHouseId} не найдена");
        _dataContext.Remove(productStoreHouseLink);
        await _dataContext.SaveChangesAsync(cancellationToken: cancellationToken);
        var newProductStoreHouseLink = _mapper.Map(request, productStoreHouseLink);
        await _dataContext.AddAsync(newProductStoreHouseLink, cancellationToken: cancellationToken);
        await _dataContext.SaveChangesAsync(cancellationToken: cancellationToken);
        return "Связь изменена";
    }
}