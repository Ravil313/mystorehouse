﻿using CQRS.Exceptions;
using CQRS.ProductStoreHouseLinks.Commands.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.ProductStoreHouseLinks.Commands.Handlers;
/// <summary>
/// Обработчик CQRS команды удаления связи товара и склада
/// </summary>
public class DeleteProductStoreHouseLinkHandler : IRequestHandler<DeleteProductStoreHouseLink, string>
{
    private readonly DataContext _dataContext;
    /// <summary>
    /// Добавление зависимостей от базы данных
    /// </summary>
    /// <param name="dataContext"></param>
    public DeleteProductStoreHouseLinkHandler(DataContext dataContext)
    {
        _dataContext = dataContext;
    }
    /// <summary>
    /// Метод обработчика CQRS команды удаления связи товара и склада
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделью из строки запроса для удаления связи</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Сообщение о успешном удалении связи</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если связь не найдена</exception>
    public async Task<string> Handle(DeleteProductStoreHouseLink request, CancellationToken cancellationToken)
    {
        var productStoreHouseLink = await _dataContext.ProductsStoreHousesData
            .FirstOrDefaultAsync(psh => psh.ProductId == request.ProductId && psh.StoreHouseId == request.StoreHouseId, cancellationToken: cancellationToken);
        if (productStoreHouseLink == null)
            throw new NotFoundException($"Связь {request.ProductId} с {request.StoreHouseId} не найдена");
        _dataContext.Remove(productStoreHouseLink);
        await _dataContext.SaveChangesAsync(cancellationToken: cancellationToken);
        return "Связь удалена";
    }
}