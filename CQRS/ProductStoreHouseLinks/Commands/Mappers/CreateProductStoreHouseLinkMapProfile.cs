﻿using AutoMapper;
using CQRS.ProductStoreHouseLinks.Commands.Models;
using DataAccessLayer.DataModels;

namespace CQRS.ProductStoreHouseLinks.Commands.Mappers;
/// <summary>
/// Автомаппер CQRS модели для команды создания связей товара и склада
/// </summary>
public class CreateProductStoreHouseLinkMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели создания связей в таблицу связей в базе данных
    /// </summary>
    public CreateProductStoreHouseLinkMapProfile()
    {
        CreateMap<CreateProductStoreHouseLink, ProductStoreHouseData>()
            .ForMember(productStoreHouseData => productStoreHouseData.ProductId, expression => expression
                .MapFrom(createProductStoreHouseLink => createProductStoreHouseLink.ProductId))
            .ForMember(productStoreHouseData => productStoreHouseData.StoreHouseId, expression => expression
                .MapFrom(createProductStoreHouseLink => createProductStoreHouseLink.StoreHouseId))
            .ForMember(productStoreHouseData => productStoreHouseData.Count, expression => expression
                .MapFrom(createProductStoreHouseLink => createProductStoreHouseLink.Count));
    }
}