﻿using AutoMapper;
using CQRS.Products.Commands.Models;
using CQRS.ProductStoreHouseLinks.Commands.Models;
using DataAccessLayer.DataModels;

namespace CQRS.ProductStoreHouseLinks.Commands.Mappers;
/// <summary>
/// Автомаппер CQRS модели для команды редактирования связей товара и склада
/// </summary>
public class EditProductStoreHouseLinkMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели редактирования связей в таблицу связей товаров и складов в базе данных
    /// </summary>
    public EditProductStoreHouseLinkMapProfile()
    {
        CreateMap<EditProductStoreHouseLink, ProductStoreHouseData>()
            .ForMember(productStoreHouseData => productStoreHouseData.ProductId, expression => expression
                .MapFrom(editProductStoreHouseLink => editProductStoreHouseLink.ToProductId))
            .ForMember(productStoreHouseData => productStoreHouseData.StoreHouseId, expression => expression
                .MapFrom(editProductStoreHouseLink => editProductStoreHouseLink.ToStoreHouseId))
            .ForMember(productStoreHouseData => productStoreHouseData.Count, expression => expression
                .MapFrom(editProductStoreHouseLink => editProductStoreHouseLink.NewCount));
    }
}