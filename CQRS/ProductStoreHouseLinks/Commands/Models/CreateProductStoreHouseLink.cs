﻿using MediatR;

namespace CQRS.ProductStoreHouseLinks.Commands.Models;
/// <summary>
/// CQRS модель для создании связи товара и склада
/// </summary>
public class CreateProductStoreHouseLink : IRequest<string>
{
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Идентификатор склада
    /// </summary>
    public int StoreHouseId { get; set; }
    /// <summary>
    /// Количество товара
    /// </summary>
    public int Count { get; set; }
}