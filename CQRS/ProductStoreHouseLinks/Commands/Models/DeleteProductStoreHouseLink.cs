﻿using MediatR;

namespace CQRS.ProductStoreHouseLinks.Commands.Models;
/// <summary>
/// CQRS модель для удаления связи товара и склада
/// </summary>
public class DeleteProductStoreHouseLink : IRequest<string>
{
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Идентификатор склада
    /// </summary>
    public int StoreHouseId { get; set; }
}