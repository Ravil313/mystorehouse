﻿using MediatR;

namespace CQRS.ProductStoreHouseLinks.Commands.Models;
/// <summary>
/// CQRS модель редактирования свзязи продукта и склада
/// </summary>
public class EditProductStoreHouseLink : IRequest<string>
{
    /// <summary>
    /// Изменяемый идентификатор товара
    /// </summary>
    public int FromProductId { get; set; }
    /// <summary>
    /// Изменяемый идентификатор склада
    /// </summary>
    public int FromStoreHouseId { get; set; }
    /// <summary>
    /// Измененный идентификатор товара
    /// </summary>
    public int ToProductId { get; set; }
    /// <summary>
    /// Измененный идентификатор склада
    /// </summary>
    public int ToStoreHouseId { get; set; }
    /// <summary>
    /// Новое количество товара
    /// </summary>
    public int NewCount { get; set; }
}