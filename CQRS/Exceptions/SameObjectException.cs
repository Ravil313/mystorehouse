﻿namespace CQRS.Exceptions;

/// <summary>
/// Обработчик ошибок: связь уже существует
/// </summary>
public class SameObjectException : Exception
{
    /// <summary>
    /// Конструктор обработчика ошибок: связь уже существует
    /// </summary>
    /// <param name="message">сообщение об ошибке</param>
    public SameObjectException(string message) : base(message)
    { }
}