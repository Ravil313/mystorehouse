﻿namespace CQRS.Exceptions;

/// <summary>
/// Обработчик ошибок: объект не найден
/// </summary>
public class NotFoundException : Exception
{
    /// <summary>
    /// Конструктор обработчика ошибок: объект не найден
    /// </summary>
    /// <param name="message">сообщение об ошибке</param>
    public NotFoundException(string message) : base(message)
    {}
}