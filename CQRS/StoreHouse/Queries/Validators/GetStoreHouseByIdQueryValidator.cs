﻿using CQRS.StoreHouse.Queries.Models;
using FluentValidation;

namespace CQRS.StoreHouse.Queries.Validators;
/// <summary>
/// Валидатор CQRS модели для получения склада по идентификатору 
/// </summary>
public class GetStoreHouseByIdQueryValidator : AbstractValidator<GetStoreHouseByIdQuery>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для получения склада по идентификатору 
    /// </summary>
    public GetStoreHouseByIdQueryValidator()
    {
        RuleFor(store => store.StoreHouseId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор склада обязателен и должен быть >= 1");
    }
}