﻿using MediatR;

namespace CQRS.StoreHouse.Queries.Models;
/// <summary>
/// CQRS Модель для поучения всех складов
/// </summary>
public class GetAllStoreHousesQuery : IRequest<IEnumerable<StoreHouseDto>>
{ }