﻿using MediatR;

namespace CQRS.StoreHouse.Queries.Models;
/// <summary>
/// CQRS модель для получения склада по идентификатору
/// </summary>
public class GetStoreHouseByIdQuery : IRequest<StoreHouseDto>
{
    /// <summary>
    /// Идентификатор склада
    /// </summary>
    public int StoreHouseId { get; set; } 
}