﻿using DataAccessLayer.DataModels;

namespace CQRS.StoreHouse.Queries.Models;
/// <summary>
/// Модель для получения данных о складе из базы данных
/// </summary>
public class StoreHouseDto
{
    /// <summary>
    /// Идентификатор склада
    /// </summary>
    public int StoreHouseId { get; set; }
    /// <summary>
    /// Название склада
    /// </summary>
    public string? StoreHouseName { get; set; }
    /// <summary>
    /// Список товаров на складе
    /// </summary>
    public ICollection<ProductsDto>? Products { get; set; }
}