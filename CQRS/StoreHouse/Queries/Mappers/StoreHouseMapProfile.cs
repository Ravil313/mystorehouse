﻿using AutoMapper;
using CQRS.StoreHouse.Queries.Models;
using DataAccessLayer.DataModels;

namespace CQRS.StoreHouse.Queries.Mappers;
/// <summary>
/// Автомаппер модели CQRS запроса для получения склада из таблицы складов в базе данных
/// </summary>
public class StoreHouseMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга модели CQRS запроса для получения склада из таблицы складов в базе данных
    /// </summary>
    public StoreHouseMapProfile()
    {
        CreateMap<StoreHouseData, StoreHouseDto>()
            .ForMember(storeHouseDto => storeHouseDto.StoreHouseId,expression => expression
                .MapFrom(storeHouseData => storeHouseData.StoreHouseId))
            .ForMember(storeHouseDto => storeHouseDto.StoreHouseName,expression => expression
                .MapFrom(storeHouseData => storeHouseData.StoreHouseName))
            .ForMember(storeHouseDto => storeHouseDto.Products,expression => expression
                .MapFrom(storeHouseData => storeHouseData.ProductStoreHouseData));
    }
}