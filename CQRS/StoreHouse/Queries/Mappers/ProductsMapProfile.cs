﻿using AutoMapper;
using CQRS.StoreHouse.Queries.Models;
using DataAccessLayer.DataModels;

namespace CQRS.StoreHouse.Queries.Mappers;
/// <summary>
/// Автомаппер модели CQRS запроса для получения товара в составе склада из таблицы связей товаров и складов в базе данных
/// </summary>
public class ProductsMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга модели CQRS запроса для получения товара в составе склада из таблицы связей товаров и складов в базе данных
    /// </summary>
    public ProductsMapProfile()
    {
        CreateMap<ProductStoreHouseData, ProductsDto>()
            .ForMember(productsDto => productsDto.ProductId,expression => expression
                .MapFrom(productStoreHouseData => productStoreHouseData.ProductId))
            .ForMember(productsDto => productsDto.ProductArticle,expression => expression
                .MapFrom(productStoreHouseData => productStoreHouseData.ProductData!.ProductDataArticle))
            .ForMember(productsDto => productsDto.ProductName,expression => expression
                .MapFrom(productStoreHouseData => productStoreHouseData.ProductData!.ProductDataName))
            .ForMember(productsDto => productsDto.LastPurchasePrice,expression => expression
                .MapFrom(productStoreHouseData => productStoreHouseData.ProductData!.LastPurchasePrice))
            .ForMember(productsDto => productsDto.LastSellPrice,expression => expression
                .MapFrom(productStoreHouseData => productStoreHouseData.ProductData!.LastSellPrice))
            .ForMember(productsDto => productsDto.Count,expression => expression
                .MapFrom(productStoreHouseData => productStoreHouseData.Count));
    }
}