﻿using AutoMapper;
using CQRS.Exceptions;
using CQRS.StoreHouse.Queries.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.StoreHouse.Queries.Handlers;
/// <summary>
/// Обработчик CQRS запроса получения склада по идентификатору
/// </summary>
public class GetStoreHouseByIdQueryHandler : IRequestHandler<GetStoreHouseByIdQuery, StoreHouseDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetStoreHouseByIdQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS запроса получения склада по идентификатору
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделю из строки запроса для получения склада по идентификатору</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Модель склада из базы данных</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если склад не найден</exception>
    public async Task<StoreHouseDto> Handle(GetStoreHouseByIdQuery request, CancellationToken cancellationToken)
    {
        var storeData = await _dataContext.StoreHousesData
            .AsNoTracking()
            .Include(store => store.ProductStoreHouseData)!
            .ThenInclude(psh => psh.ProductData)
            .FirstOrDefaultAsync(store => store.StoreHouseId == request.StoreHouseId, cancellationToken: cancellationToken);
        if (storeData == null)
            throw new NotFoundException($"Склад с артикулом: {request.StoreHouseId} не найден");
        return _mapper.Map<StoreHouseDto>(storeData);
    }
}