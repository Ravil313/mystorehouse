﻿using AutoMapper;
using CQRS.Exceptions;
using CQRS.StoreHouse.Queries.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.StoreHouse.Queries.Handlers;
/// <summary>
/// Обработчик CQRS запроса получения всех складов
/// </summary>
public class GetAllStoreHousesQueryHandler : IRequestHandler<GetAllStoreHousesQuery, IEnumerable<StoreHouseDto>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetAllStoreHousesQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS запроса получения всех складов
    /// </summary>
    /// <param name="request">Запрос с запролненной CQRS моделью для получения всех складов</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Коллекцию складов из базы данных</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если список складов пуст</exception>
    public async Task<IEnumerable<StoreHouseDto>> Handle(GetAllStoreHousesQuery request, CancellationToken cancellationToken)
    {
        var storeData = await _dataContext.StoreHousesData
            .AsNoTracking()
            .Include(store => store.ProductStoreHouseData)!
            .ThenInclude(psh => psh.ProductData)
            .ToListAsync(cancellationToken: cancellationToken);
        if (storeData == null)
            throw new NotFoundException($"Список складов пуст");
        return _mapper.Map<IEnumerable<StoreHouseDto>>(storeData);
    }
}