﻿using AutoMapper;
using CQRS.StoreHouse.Commands.Models;
using DataAccessLayer.DataModels;

namespace CQRS.StoreHouse.Commands.Mappers;
/// <summary>
/// Автомаппер CQRS модели для команды создания склада
/// </summary>
public class CreateStoreHouseMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели создания склада в таблицу складов в базе данных
    /// </summary>
    public CreateStoreHouseMapProfile()
    {
        CreateMap<CreateStoreHouseCommand, StoreHouseData>()
            .ForMember(storeHouseData => storeHouseData.StoreHouseName,
                expression => expression.MapFrom(createStoreHouseCommand => createStoreHouseCommand.StoreHouseName));
    }
}