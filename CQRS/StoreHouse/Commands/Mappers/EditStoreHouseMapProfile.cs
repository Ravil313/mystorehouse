﻿using AutoMapper;
using CQRS.StoreHouse.Commands.Models;
using DataAccessLayer.DataModels;

namespace CQRS.StoreHouse.Commands.Mappers;
/// <summary>
/// Автомаппер CQRS модели для команды редактирования склада
/// </summary>
public class EditStoreHouseMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели редактирования склада в таблицу складов в базе данных
    /// </summary>
    public EditStoreHouseMapProfile()
    {
        CreateMap<EditStoreHouseCommand, StoreHouseData>()
            .ForMember(storeHouseData => storeHouseData.StoreHouseName,
                expression => expression.MapFrom(editStoreHouseCommand => editStoreHouseCommand.NewStoreHouseName));
    }
}