﻿using AutoMapper;
using CQRS.StoreHouse.Commands.Models;
using DataAccessLayer;
using DataAccessLayer.DataModels;
using MediatR;

namespace CQRS.StoreHouse.Commands.Handlers;
/// <summary>
/// Обработчик CQRS команды создания склада
/// </summary>
public class CreateStoreHouseCommandHandler : IRequestHandler<CreateStoreHouseCommand, int>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public CreateStoreHouseCommandHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS команды создания склада
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделью из тела запроса для создания склада</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Идентификатор склада</returns>
    public async Task<int> Handle(CreateStoreHouseCommand request, CancellationToken cancellationToken)
    {
        var storeHouse = _mapper.Map<StoreHouseData>(request);
        await _dataContext.StoreHousesData.AddAsync(storeHouse, cancellationToken : cancellationToken);
        await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        return storeHouse.StoreHouseId;
    }
}