﻿using AutoMapper;
using CQRS.Exceptions;
using CQRS.StoreHouse.Commands.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.StoreHouse.Commands.Handlers;
/// <summary>
/// Обработчик CQRS команды редактирования склада
/// </summary>
public class EditStoreHouseCommandHandler : IRequestHandler<EditStoreHouseCommand, string>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public EditStoreHouseCommandHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS команды редактирования склада
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделью из тела запроса для редактирования склада</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Сообщение о успешном редактировании склада</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если склад не найден/exception></exception>
    public async Task<string> Handle(EditStoreHouseCommand request, CancellationToken cancellationToken)
    {
        var storeHouseData = await _dataContext.StoreHousesData
            .FirstOrDefaultAsync(store => store.StoreHouseId == request.StoreHouseId, cancellationToken);
        if (storeHouseData == null) throw new NotFoundException($"Склад с артикулом: {request.StoreHouseId} не найден");
        _mapper.Map(request, storeHouseData);
        await _dataContext.SaveChangesAsync(cancellationToken);
        return "Склад изменен";
    }
}