﻿using CQRS.Exceptions;
using CQRS.StoreHouse.Commands.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.StoreHouse.Commands.Handlers;
/// <summary>
/// Обработчик CQRS команды удаления склада
/// </summary>
public class DeleteStoreHouseCommandHandler : IRequestHandler<DeleteStoreHouseCommand, string>
{
    private readonly DataContext _dataContext;
    /// <summary>
    /// Добавление зависимостей от базы данных
    /// </summary>
    /// <param name="dataContext"></param>
    public DeleteStoreHouseCommandHandler(DataContext dataContext)
    {
        _dataContext = dataContext;
    }
    /// <summary>
    /// Метод обработчика CQRS команды удаления склада
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделью из строки запроса для удаления склада</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Сообщение о успешном удалении склада</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если склад не найден</exception>
    public async Task<string> Handle(DeleteStoreHouseCommand request, CancellationToken cancellationToken)
    {
        var storeHouseData = await _dataContext.StoreHousesData
            .FirstOrDefaultAsync(store => store.StoreHouseId == request.StoreHouseId, cancellationToken);
        if (storeHouseData == null) throw new NotFoundException($"Склад с артикулом: {request.StoreHouseId} не найден");
        _dataContext.Remove(storeHouseData);
        await _dataContext.SaveChangesAsync(cancellationToken);
        return "Склад удален";
    }
}