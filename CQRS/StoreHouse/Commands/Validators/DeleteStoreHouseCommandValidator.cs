﻿using CQRS.StoreHouse.Commands.Models;
using FluentValidation;

namespace CQRS.StoreHouse.Commands.Validators;
/// <summary>
/// Валидатор CQRS модели для удаления склада
/// </summary>
public class DeleteStoreHouseCommandValidator : AbstractValidator<DeleteStoreHouseCommand>
{
    /// <summary>
    /// Правила валидации полей модели для удаления склада
    /// </summary>
    public DeleteStoreHouseCommandValidator()
    {
        RuleFor(pro => pro.StoreHouseId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор склада обязателен и должен быть >= 1");
    }
}