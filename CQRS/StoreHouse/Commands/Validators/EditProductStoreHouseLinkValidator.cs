﻿using CQRS.StoreHouse.Commands.Models;
using FluentValidation;

namespace CQRS.StoreHouse.Commands.Validators;
/// <summary>
/// Валидатор CQRS модели для редактирования склада
/// </summary>
public class EditProductStoreHouseLinkValidator : AbstractValidator<EditStoreHouseCommand>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для редактирования склада
    /// </summary>
    public EditProductStoreHouseLinkValidator()
    {
        RuleFor(pro => pro.StoreHouseId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор склада обязателен и должен быть >= 1");
        RuleFor(store => store.NewStoreHouseName)
            .NotEmpty()
            .NotNull()
            .MaximumLength(100)
            .WithMessage("Название склада обязательно и не более 100 символов");
    }
}