﻿using CQRS.StoreHouse.Commands.Models;
using FluentValidation;

namespace CQRS.StoreHouse.Commands.Validators;
/// <summary>
/// Валидатор CQRS модели для создания склада
/// </summary>
public class CreateStoreHouseCommandValidator : AbstractValidator<CreateStoreHouseCommand>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для создания склада
    /// </summary>
    public CreateStoreHouseCommandValidator()
    {
        RuleFor(store => store.StoreHouseName)
            .NotEmpty()
            .NotNull()
            .MaximumLength(100)
            .WithMessage("Название склада обязательно и не более 100 символов");
    }
}