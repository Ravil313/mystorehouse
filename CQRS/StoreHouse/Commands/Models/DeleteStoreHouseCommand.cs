﻿using MediatR;

namespace CQRS.StoreHouse.Commands.Models;
/// <summary>
/// CQRS модель для удаления склада
/// </summary>
public class DeleteStoreHouseCommand : IRequest<string>
{
    /// <summary>
    /// Идентификатор склада
    /// </summary>
    public int StoreHouseId { get; set; }
}