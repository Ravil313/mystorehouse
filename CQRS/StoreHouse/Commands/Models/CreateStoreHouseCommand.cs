﻿using MediatR;

namespace CQRS.StoreHouse.Commands.Models;
/// <summary>
/// CQRS модель для создания склада
/// </summary>
public class CreateStoreHouseCommand : IRequest<int>
{
    /// <summary>
    /// Название склада
    /// </summary>
    public string? StoreHouseName { get; set; }
}