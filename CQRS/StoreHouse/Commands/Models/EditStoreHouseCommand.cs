﻿using MediatR;

namespace CQRS.StoreHouse.Commands.Models;
/// <summary>
/// CQRS модель для редактирования склада
/// </summary>
public class EditStoreHouseCommand : IRequest<string>
{
    /// <summary>
    /// Идентификатор изменяемого склада
    /// </summary>
    public int StoreHouseId { get; set; }
    /// <summary>
    /// Новое название изменяемого склада
    /// </summary>
    public string? NewStoreHouseName { get; set; }
}