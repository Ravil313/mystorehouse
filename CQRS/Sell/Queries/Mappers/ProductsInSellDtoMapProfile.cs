﻿using AutoMapper;
using CQRS.Sell.Queries.Models;
using DataAccessLayer.DataModels;

namespace CQRS.Sell.Queries.Mappers;
/// <summary>
/// Автомаппер CQRS модели для запроса получения продукта в составе продажи с моделью продуктов в базе дыных из таблицы связей продажи и продуктов
/// </summary>
public class ProductsInSellDtoMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели для запроса получения продукта в составе продажи с моделью продуктов в базе дыных из таблицы связей продажи и продуктов
    /// </summary>
    public ProductsInSellDtoMapProfile()
    {
        CreateMap<SellProductsData, ProductsInSellDto>()
            .ForMember(productsInSellDto => productsInSellDto.ProductId,expression => expression
                .MapFrom(sellProductsData => sellProductsData.ProductId))
            .ForMember(productsInSellDto => productsInSellDto.ProductName,expression => expression
                .MapFrom(sellProductsData => sellProductsData.ProductData!.ProductDataName))
            .ForMember(productsInSellDto => productsInSellDto.SellPrice,expression => expression
                .MapFrom(sellProductsData => sellProductsData.SellId))
            .ForMember(productsInSellDto => productsInSellDto.Count,expression => expression
                .MapFrom(sellProductsData => sellProductsData.Count));
    }
}