﻿using AutoMapper;
using CQRS.Sell.Queries.Models;
using DataAccessLayer.DataModels;

namespace CQRS.Sell.Queries.Mappers;
/// <summary>
/// Автомаппер модели CQRS запроса для получения продажи из таблицы продаж в базе дынных
/// </summary>
public class SellDtoMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга модели CQRS запроса для получения продажи из таблицы продаж в базе дынных
    /// </summary>
    public SellDtoMapProfile()
    {
        CreateMap<SellData, SellDto>()
            .ForMember(sellDto => sellDto.SellId,expression => expression.MapFrom(sellData => sellData.SellId))
            .ForMember(sellDto => sellDto.SellNumber,expression => expression.MapFrom(sellData => sellData.SellNumber))
            .ForMember(sellDto => sellDto.StoreHouseId,expression => expression.MapFrom(sellData => sellData.StoreHouseId))
            .ForMember(sellDto => sellDto.Date,expression => expression.MapFrom(sellData => sellData.Date))
            .ForMember(sellDto => sellDto.ProductsInSell,expression => expression.MapFrom(sellData => sellData.SellProducts));
    }
}