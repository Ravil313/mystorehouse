﻿using CQRS.Sell.Queries.Models;
using FluentValidation;

namespace CQRS.Sell.Queries.Validators;
/// <summary>
/// Валидатор CQRS модели для получения продажи по номеру 
/// </summary>
public class GetSellByNumberQueryValidator : AbstractValidator<GetSellByNumberQuery>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для получения продажи по номеру 
    /// </summary>
    public GetSellByNumberQueryValidator()
    {
        RuleFor(sell => sell.SellNumber)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Номер продажи обязателен и должен быть >= 1");
    }
}