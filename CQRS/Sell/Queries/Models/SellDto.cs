﻿namespace CQRS.Sell.Queries.Models;
/// <summary>
/// Модель для получения данных о продаже товаров из базы данных
/// </summary>
public class SellDto
{
    /// <summary>
    /// Идентификатор продажи
    /// </summary>
    public int SellId { get; set; }
    /// <summary>
    /// Номер продажи
    /// </summary>
    public int SellNumber { get; set; }
    /// <summary>
    /// Идентификатор склада
    /// </summary>
    public int StoreHouseId { get; set; }
    /// <summary>
    /// Дата продажи
    /// </summary>
    public string? Date { get; set; }
    /// <summary>
    /// Список товаров в продаже
    /// </summary>
    public ICollection<ProductsInSellDto>? ProductsInSell { get; set; }
}