﻿using MediatR;

namespace CQRS.Sell.Queries.Models;
/// <summary>
/// CQRS Модель для поучения всех продаж
/// </summary>
public class GetAllSellsQuery : IRequest<IEnumerable<SellDto>>
{ }