﻿namespace CQRS.Sell.Queries.Models;
/// <summary>
/// Модель для получения данных о товарах в таблице продажи из базы данных
/// </summary>
public class ProductsInSellDto
{
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Название товара
    /// </summary>
    public string? ProductName { get; set; }
    /// <summary>
    /// Закупочная цена продажи товара
    /// </summary>
    public int SellPrice { get; set; }
    /// <summary>
    /// Количество товара в продаже
    /// </summary>
    public int Count { get; set; }
}