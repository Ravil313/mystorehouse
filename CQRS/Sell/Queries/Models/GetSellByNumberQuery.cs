﻿using MediatR;

namespace CQRS.Sell.Queries.Models;
/// <summary>
/// CQRS модель для получения продажи по номеру
/// </summary>
public class GetSellByNumberQuery : IRequest<SellDto>
{
    /// <summary>
    /// Номер продажи
    /// </summary>
    public int SellNumber { get; set; } 
}