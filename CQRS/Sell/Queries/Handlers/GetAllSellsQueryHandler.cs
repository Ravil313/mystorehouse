﻿using AutoMapper;
using CQRS.Admission.Queries.Models;
using CQRS.Exceptions;
using CQRS.Sell.Queries.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Sell.Queries.Handlers;
/// <summary>
/// Обработчик CQRS запроса получения всех продаж
/// </summary>
public class GetAllSellsQueryHandler : IRequestHandler<GetAllSellsQuery, IEnumerable<SellDto>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetAllSellsQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS запроса получения всех продаж
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделю из строки запроса для получения всех продаж</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Коллекция всех продаж</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки список продаж пуст</exception>
    public async Task<IEnumerable<SellDto>> Handle(GetAllSellsQuery request, CancellationToken cancellationToken)
    {
        var sellsData = await _dataContext.SellsData
            .AsNoTracking()
            .Include(sell => sell.SellProducts)!
            .ThenInclude(sellProductsData => sellProductsData.ProductData)
            .ToListAsync(cancellationToken);
        if (sellsData == null)
            throw new NotFoundException("Список продаж пуст");
        return _mapper.Map<IEnumerable<SellDto>>(sellsData);
    }
}