﻿using AutoMapper;
using CQRS.Exceptions;
using CQRS.Sell.Queries.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Sell.Queries.Handlers;
/// <summary>
/// Обработчик CQRS запроса получения продажи по номеру
/// </summary>
public class GetSellByNumberQueryHandler : IRequestHandler<GetSellByNumberQuery, SellDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetSellByNumberQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS запроса получения продажи по номеру
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделю из строки запроса для получения продажи по номеру</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Модель продажи из базы данных</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если продажа не найдена</exception>
    public async Task<SellDto> Handle(GetSellByNumberQuery request, CancellationToken cancellationToken)
    {
        var sellData = await _dataContext.SellsData
            .AsNoTracking()
            .Include(sell => sell.SellProducts)!
            .ThenInclude(sellProductsData => sellProductsData.ProductData)
            .FirstOrDefaultAsync(sell => sell.SellNumber == request.SellNumber, cancellationToken: cancellationToken);
        if (sellData == null)
            throw new NotFoundException($"Продажа с номером: {request.SellNumber} не найдена");
        return _mapper.Map<SellDto>(sellData);
    }
}