﻿namespace CQRS.Sell.Commands;
/// <summary>
/// Модель для создания связи товаров в продаже
/// </summary>
public class ProductsInSellCommand
{
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Цена продажи товара
    /// </summary>
    public int SellPrice { get; set; }
    /// <summary>
    /// Количество товара в продаже
    /// </summary>
    public int Count { get; set; }
}