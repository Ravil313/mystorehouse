﻿using MediatR;

namespace CQRS.Sell.Commands;
/// <summary>
/// CQRS модель для создания продажи
/// </summary>
public class CreateSellCommand : IRequest<int>
{
    /// <summary>
    /// Номер продажи
    /// </summary>
    public int SellNumber { get; set; }
    /// <summary>
    /// Идентификатор склада для продажи
    /// </summary>
    public int StoreHouseId { get; set; }
    /// <summary>
    /// Дата продажи
    /// </summary>
    public string? Date { get; set; }
    /// <summary>
    /// Список товаров в продаже
    /// </summary>
    public ICollection<ProductsInSellCommand>? ProductsInSell { get; set; }
}