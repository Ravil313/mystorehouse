﻿using AutoMapper;
using DataAccessLayer.DataModels;

namespace CQRS.Sell.Commands;
/// <summary>
/// Автомаппер CQRS модели для команды добавления товаров в продажу
/// </summary>
public class ProductsInSellCommandMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели для команды добавления товаров в продажу
    /// </summary>
    public ProductsInSellCommandMapProfile()
    {
        CreateMap<ProductsInSellCommand, SellProductsData>()
            .ForMember(sellProductsData => sellProductsData.ProductId,
                expression => expression.MapFrom(productsInSellCommand => productsInSellCommand.ProductId))
            .ForMember(sellProductsData => sellProductsData.SellPrice,
                expression => expression.MapFrom(productsInSellCommand => productsInSellCommand.SellPrice))
            .ForMember(sellProductsData => sellProductsData.Count,
                expression => expression.MapFrom(productsInSellCommand => productsInSellCommand.Count));
    }
}