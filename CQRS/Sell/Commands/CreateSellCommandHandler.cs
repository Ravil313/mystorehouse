﻿using AutoMapper;
using CQRS.CustomCheckers;
using CQRS.Exceptions;
using DataAccessLayer;
using DataAccessLayer.DataModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Sell.Commands;
/// <summary>
/// Обработчик CQRS команды создания продажи
/// </summary>
public class CreateSellCommandHandler : IRequestHandler<CreateSellCommand, int>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    private readonly ICustomChecker<CreateSellCommand> _checker;
    /// <summary>
    /// Добавление зависимостей от базы данных, автомаппера и медиатора
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public CreateSellCommandHandler(DataContext dataContext, IMapper mapper, ICustomChecker<CreateSellCommand> checker)
    {
        _dataContext = dataContext;
        _mapper = mapper;
        _checker = checker;
    }
    /// <summary>
    /// Метод обработчика CQRS команды создания продажи
    /// </summary>
    /// <param name="request">Запрос содержащий в теле заполненную CQRS модель для создания продажи</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    /// <exception cref="NotFoundException">Возникновение ошибки в случае не существования склада или товара необходимые для создания продажи</exception>
    public async Task<int> Handle(CreateSellCommand request, CancellationToken cancellationToken)
    {
        await _checker.CommandRequestChecker(request, cancellationToken);
        try
        { 
            await EditLinkInProductStoreHouseData(request, cancellationToken);
        }
        catch (ArgumentOutOfRangeException e)
        {
            Console.WriteLine(e);
            throw;
        }
        var sellData = _mapper.Map<SellData>(request);
        await _dataContext.SellsData.AddAsync(sellData, cancellationToken : cancellationToken);
        await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        foreach (var productInSell in request.ProductsInSell!)
        {
            var sellProductData = _mapper.Map<SellProductsData>(productInSell);
            sellProductData.SellId = sellData.SellId;
            await _dataContext.SellProductsData.AddAsync(sellProductData, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        }
        await UpdateProductDataLastSellPrice(request, cancellationToken);
        return sellData.SellId;
    }
    /// <summary>
    /// Обновление последней закупочной цены продукта
    /// </summary>
    /// <param name="request">Запрос</param>
    /// <param name="cancellationToken"></param>
    private async Task UpdateProductDataLastSellPrice(CreateSellCommand request, CancellationToken cancellationToken)
    {
        foreach (var productInSell in request.ProductsInSell!)
        {
            var productData = await _dataContext.ProductsData
                .FirstOrDefaultAsync(product => product.ProductId == productInSell.ProductId, cancellationToken);
            productData!.LastSellPrice = productInSell.SellPrice;
            await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        }
    }
    /// <summary>
    /// Редактирование связи в таблице связей продукта и склада
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <exception cref="ArgumentOutOfRangeException">Возникновение ошибки при попытке продать больше товара, чем есть на складе</exception>
    private async Task EditLinkInProductStoreHouseData(CreateSellCommand request, CancellationToken cancellationToken)
    {
        foreach (var productInSell in request.ProductsInSell!)
        {
            var productStoreHouse = await _dataContext.ProductsStoreHousesData
                .FirstOrDefaultAsync(productStore => productStore.StoreHouseId == request.StoreHouseId && productStore.ProductId == productInSell.ProductId, cancellationToken);
            if (productStoreHouse!.Count < productInSell.Count)
                throw new ArgumentOutOfRangeException(nameof(productInSell.Count), "Количество продоваемого товара больше, чем есть на складе");
            if (productStoreHouse.Count == productInSell.Count)
            {
                _dataContext.ProductsStoreHousesData.Remove(productStoreHouse);
                await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
            }
            else
            {
                _dataContext.ProductsStoreHousesData.Remove(productStoreHouse);
                await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
                productStoreHouse.Count -= productInSell.Count;
                await _dataContext.ProductsStoreHousesData.AddAsync(productStoreHouse, cancellationToken);
                await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
            }
        }
    }
}