﻿using AutoMapper;
using DataAccessLayer.DataModels;

namespace CQRS.Sell.Commands;
/// <summary>
/// Автомаппер CQRS модели для команды создания продажи
/// </summary>
public class CreateSellCommandMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели создания продажи в таблицу продаж в базе данных
    /// </summary>
    public CreateSellCommandMapProfile()
    {
        CreateMap<CreateSellCommand, SellData>()
            .ForMember(sellData => sellData.SellNumber,
                expression => expression.MapFrom(createSellCommand => createSellCommand.SellNumber))
            .ForMember(sellData => sellData.StoreHouseId,
                expression => expression.MapFrom(createSellCommand => createSellCommand.StoreHouseId))
            .ForMember(sellData => sellData.Date,
                expression => expression.MapFrom(createSellCommand => createSellCommand.Date));
    }
}