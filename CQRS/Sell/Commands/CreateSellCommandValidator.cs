﻿using FluentValidation;

namespace CQRS.Sell.Commands;
/// <summary>
/// Валидатор CQRS модели для создания продажи
/// </summary>
public class CreateSellCommandValidator : AbstractValidator<CreateSellCommand>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для создания продажи
    /// </summary>
    public CreateSellCommandValidator()
    {
        RuleFor(createSellCommand => createSellCommand.SellNumber)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Номер продажи обязателен и должен быть >= 1");
        RuleFor(createSellCommand => createSellCommand.StoreHouseId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор склада обязателен и должен быть >= 1");
        RuleFor(createSellCommand => createSellCommand.Date)
            .NotEmpty()
            .NotNull()
            .MaximumLength(10)
            .WithMessage("Дата обязательна и не более 10 символов формата: 01.01.2020");
        RuleForEach(createSellCommand => createSellCommand.ProductsInSell)
            .SetValidator(new ProductsInSellCommandValidator());
    }
}