﻿using FluentValidation;

namespace CQRS.Sell.Commands;
/// <summary>
/// Валидатор CQRS модели для добавления продуктов в продажу
/// </summary>
public class ProductsInSellCommandValidator : AbstractValidator<ProductsInSellCommand>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для добавления продуктов в продажу
    /// </summary>
    public ProductsInSellCommandValidator()
    {
        RuleFor(productsInSellCommand => productsInSellCommand.ProductId)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Идентификатор товара обязателен и должен быть >= 1");
        RuleFor(productsInSellCommand => productsInSellCommand.SellPrice)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Цена продажи товара обязательна и должена быть >= 1");
        RuleFor(productsInSellCommand => productsInSellCommand.Count)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Количество товара обязателено и должено быть >= 1");
    }
}