﻿using DataAccessLayer.DataModels;

namespace CQRS.Users;
/// <summary>
/// Покупатель
/// </summary>
public class UserDto
{
    /// <summary>
    /// Идентификатор
    /// </summary>
    public string Id { get; set; }
    /// <summary>
    /// Имя пользователя
    /// </summary>
    public string? FirstName { get; set; }
    /// <summary>
    /// Фамилия пользователя
    /// </summary>
    public string? LastName { get; set; }
    /// <summary>
    /// Уровень доступа (роль)
    /// </summary>
    public Roles Role { get; set; }
    /// <summary>
    /// Номер телефона
    /// </summary>
    public string? PhoneNumber { get; set; }
}