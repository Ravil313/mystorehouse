﻿using AutoMapper;
using DataAccessLayer.DataModels;

namespace CQRS.Users;

/// <summary>
/// Автомаппер для пользователя
/// </summary>
public class UserMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга пользователя
    /// </summary>
    public UserMapProfile()
    {
        CreateMap<UserData, UserDto>()
            .ForMember(dto => dto.Id, exception => exception.MapFrom(userData => userData.Id))
            .ForMember(dto => dto.FirstName, exception => exception.MapFrom(userData => userData.FirstName))
            .ForMember(dto => dto.LastName, exception => exception.MapFrom(userData => userData.LastName))
            .ForMember(dto => dto.Role, exception => exception.MapFrom(userData => (int)userData.Role))
            .ForMember(dto => dto.PhoneNumber, exception => exception.MapFrom(userData => userData.PhoneNumber));
        
        CreateMap<UserDto, UserData>()
            .ForMember(userData => userData.Id, exception => exception.MapFrom(dto => dto.Id))
            .ForMember(userData => userData.FirstName, exception => exception.MapFrom(dto => dto.FirstName))
            .ForMember(userData => userData.LastName, exception => exception.MapFrom(dto => dto.LastName))
            .ForMember(userData => userData.Role, exception => exception.MapFrom(dto => (int)dto.Role))
            .ForMember(userData => userData.PhoneNumber, exception => exception.MapFrom(dto => dto.PhoneNumber));
    }
}