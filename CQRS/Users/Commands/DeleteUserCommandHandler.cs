﻿using CQRS.Exceptions;
using DataAccessLayer;
using MediatR;

namespace CQRS.Users.Commands;

/// <summary>
/// CQRS команда удаления пользователя по идентификатору
/// </summary>
public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, int>
{
    private readonly DataContext _dataContext;

    /// <summary>
    /// Добавление зависимости от базы данных
    /// </summary>
    /// <param name="dataContext"></param>
    public DeleteUserCommandHandler(DataContext dataContext)
    {
        _dataContext = dataContext;
    }

    /// <summary>
    /// CQRS обработчик команды удаления пользователя по идентификатору
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Идентификатор удаленного пользователя</returns>
    /// <exception cref="NotFoundException">Ошибка: пользователь с указанным идентификатором не найден</exception>
    public async Task<int> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
    {
        var userData = _dataContext.UsersData
            .FirstOrDefault(user1=> user1.Id == request.Id);
        
        if (userData != null)
        {
            _dataContext.UsersData.Remove(userData);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return userData.Id;
        }
        throw new NotFoundException($"Пользователь с Id: {request.Id} не найден");
    }
}