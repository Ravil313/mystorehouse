﻿using DataAccessLayer.DataModels;
using MediatR;

namespace CQRS.Users.Commands;
/// <summary>
/// CQRS vодель для измененя данных пользователя
/// </summary>
public class EditUserCommand : IRequest<string>
{
    /// <summary>
    /// Идентификатор пользователя
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// Имя пользователя
    /// </summary>
    public string? FirstName { get; set; }
    /// <summary>
    /// Фамилия пользователя
    /// </summary>
    public string? LastName { get; set; }
    /// <summary>
    /// Уровень доступа пользователя
    /// </summary>
    public Roles Role { get; set; }
    /// <summary>
    /// Номер телефона пользователя
    /// </summary>
    public string? PhoneNumber { get; set; }
}