﻿using DataAccessLayer.DataModels;
using MediatR;

namespace CQRS.Users.Commands;
/// <summary>
/// CQRS модель создания пользователя
/// </summary>
public class CreateUserCommand : IRequest<int>
{
    /// <summary>
    /// Логин пользователя
    /// </summary>
    public string Login { get; set; }
    /// <summary>
    /// Пароль пользователя
    /// </summary>
    public string Password { get; set; }
    /// <summary>
    /// Имя пользователя
    /// </summary>
    public string? FirstName { get; set; }
    /// <summary>
    /// Фамиля пользователя
    /// </summary>
    public string? LastName { get; set; }
    /// <summary>
    /// Номер телефона пользователя
    /// </summary>
    public string? PhoneNumber { get; set; }
    /// <summary>
    /// Уровень доступа
    /// </summary>
    public Roles Role { get; set; }
}