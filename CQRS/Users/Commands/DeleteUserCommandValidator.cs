﻿using FluentValidation;

namespace CQRS.Users.Commands;

/// <summary>
/// Валидатор CQRS модели удаления пользователя по идентификатору
/// </summary>
public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
{
    /// <summary>
    /// Правило валидации CQRS модели удаления пользователя по идентификатору
    /// </summary>
    public DeleteUserCommandValidator()
    {
        RuleFor(command => command.Id).NotNull().GreaterThanOrEqualTo(0);
    }
}