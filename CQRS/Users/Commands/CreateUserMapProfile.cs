﻿using AutoMapper;
using DataAccessLayer.DataModels;

namespace CQRS.Users.Commands;

/// <summary>
/// Автомаппер CQRS модели создания пользователя и модели в базе данных
/// </summary>
public class CreateUserMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели создания пользователя и модели в базе данных
    /// </summary>
    public CreateUserMapProfile()
    {
        CreateMap<CreateUserCommand, UserData>()
            .ForMember(userData => userData.Login, expression => expression.MapFrom(command => command.Login))
            .ForMember(userData => userData.Password, expression => expression.MapFrom(command => command.Password))
            .ForMember(userData => userData.FirstName, expression => expression.MapFrom(command => command.FirstName))
            .ForMember(userData => userData.LastName, expression => expression.MapFrom(command => command.LastName))
            .ForMember(userData => userData.Role, expression => expression.MapFrom(command => (int)command.Role))
            .ForMember(userData => userData.PhoneNumber, expression => expression.MapFrom(command => command.PhoneNumber));
    }
}