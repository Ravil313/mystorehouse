﻿using MediatR;

namespace CQRS.Users.Commands;

/// <summary>
/// CQRS модель для удаления пользователя из базы
/// </summary>
public class DeleteUserCommand : IRequest<int>
{
    /// <summary>
    /// Идентификатор пользователя в базе
    /// </summary>
    public int Id { get; set; }
}
    