﻿using AutoMapper;
using DataAccessLayer.DataModels;

namespace CQRS.Users.Commands;

/// <summary>
/// Автомаппер CQRS модели изменения данных пользователя
/// </summary>
public class EditUserMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели изменеия данных пользователя и модели в базе данных
    /// </summary>
    public EditUserMapProfile()
    {
        CreateMap<EditUserCommand, UserData>()
            .ForMember(userData => userData.Id, expression => expression.MapFrom(command => command.Id))
            .ForMember(userData => userData.FirstName, expression => expression.MapFrom(command => command.FirstName))
            .ForMember(userData => userData.LastName, expression => expression.MapFrom(command => command.LastName))
            .ForMember(userData => userData.Role, expression => expression.MapFrom(command => (int)command.Role))
            .ForMember(userData => userData.PhoneNumber, expression => expression.MapFrom(command => command.PhoneNumber));
        
    }
}