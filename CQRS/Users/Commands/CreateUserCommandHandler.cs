﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.DataModels;
using MediatR;

namespace CQRS.Users.Commands;

/// <summary>
/// CQRS команда создания пользователя
/// </summary>
public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, int>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public CreateUserCommandHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик команды создания пользователя
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Идентификатор нового пользователя</returns>
    public async Task<int> Handle(CreateUserCommand request, CancellationToken cancellationToken)
    {
        var userData =  _mapper.Map<UserData>(request);
        await _dataContext.UsersData.AddAsync(userData, cancellationToken);
        await _dataContext.SaveChangesAsync(cancellationToken);
        return userData.Id;
    }
}