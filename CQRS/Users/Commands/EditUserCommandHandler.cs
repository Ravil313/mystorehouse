﻿using AutoMapper;
using CQRS.Exceptions;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Users.Commands;
/// <summary>
/// CQRS команда для обновления данных пользователя
/// </summary>
public class EditUserCommandHandler : IRequestHandler<EditUserCommand, string>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Добавление зависимойстей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public EditUserCommandHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик команды изменения данных пользователя
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Имя пользователя</returns>
    /// <exception cref="NotFoundException">Ошибка: пользователь с указанным идентификаторм не найден</exception>
    /// <exception cref="InvalidOperationException">Ошибка: отсутствует имя пользователя с указанным иденитификатором</exception>
    public async Task<string> Handle(EditUserCommand request, CancellationToken cancellationToken)
    {
        var userData = await _dataContext.UsersData
            .FirstOrDefaultAsync(user1=> user1.Id == request.Id, cancellationToken : cancellationToken);
        
        if (userData == null) 
            throw new NotFoundException($"Пользователь с Id: {request.Id} не найден");
        
        _mapper.Map(request, userData);
        await _dataContext.SaveChangesAsync(cancellationToken);
        return userData.FirstName ?? throw new InvalidOperationException();
            
    }
}