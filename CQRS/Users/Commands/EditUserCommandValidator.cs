﻿using FluentValidation;

namespace CQRS.Users.Commands;

/// <summary>
/// Валидатор CQRS модели изменения данных пользователя
/// </summary>
public class EditUserCommandValidator : AbstractValidator<EditUserCommand>
{
    /// <summary>
    /// Правила валидации CQRS модели изменения данных пользователя
    /// </summary>
    public EditUserCommandValidator()
    {
        RuleFor(command => command.Id).NotNull().GreaterThanOrEqualTo(0);
        RuleFor(command => command.FirstName).NotNull().NotEmpty().Length(3, 20);
        RuleFor(command => command.LastName).NotNull().NotEmpty().Length(3, 20);
        RuleFor(command => command.Role).NotNull().NotEmpty().IsInEnum();
    }
}