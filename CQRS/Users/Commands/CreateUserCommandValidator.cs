﻿using FluentValidation;

namespace CQRS.Users.Commands;

/// <summary>
/// Валидатор CQRS модели создания пользователя
/// </summary>
public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
{
    /// <summary>
    /// Правила валидации CQRS модели создания пользователя
    /// </summary>
    public CreateUserCommandValidator()
    {
        RuleFor(command => command.Login).NotNull().NotEmpty().Length(3, 20);
        RuleFor(command => command.Password).NotNull().NotEmpty().Length(3, 20);
        RuleFor(command => command.FirstName).NotNull().NotEmpty().Length(3, 20);
        RuleFor(command => command.LastName).NotNull().NotEmpty().Length(3, 20);
        RuleFor(command => command.PhoneNumber).NotNull().NotEmpty();
        RuleFor(command => command.Role).NotNull().NotEmpty().IsInEnum();
    }
}