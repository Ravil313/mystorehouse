﻿using MediatR;

namespace CQRS.Users.Queries;

/// <summary>
/// Модель для получения пользователя по идентификатору
/// </summary>
public class GetUserQuery : IRequest<UserDto>
{
    /// <summary>
    /// Поля для идентификатора пользователя
    /// </summary>
    public int Id { get; set; }
}