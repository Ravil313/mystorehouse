﻿using FluentValidation;

namespace CQRS.Users.Queries;

/// <summary>
/// Валидация для модели GetUserQuery - поиска пользователя по идентификатору
/// </summary>
public class GetUserQueryValidator : AbstractValidator<GetUserQuery>
{
    /// <summary>
    /// Правило длая модели GetUserQuery
    /// </summary>
    public GetUserQueryValidator()
    {
        RuleFor(q => q.Id).GreaterThanOrEqualTo(0);
    }
}