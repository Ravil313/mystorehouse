﻿using AutoMapper;
using CQRS.Exceptions;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Users.Queries;

/// <summary>
/// Обработчик запроса получения пользователя по идентификатору
/// </summary>
public class GetUserQueryHandler : IRequestHandler<GetUserQuery, UserDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetUserQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// Метод обработчика запроса получения пользователя по идентификатору
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Пользователя из базы данных</returns>
    /// <exception cref="NotFoundException">Ошибка: пользователя с указанным идентификатором не сушествует</exception>
    public async Task<UserDto> Handle(GetUserQuery request, CancellationToken cancellationToken)
    {
        var user = await _dataContext.UsersData
            .AsNoTracking()
            .FirstOrDefaultAsync(u => u.Id == request.Id, cancellationToken: cancellationToken);
        if (user == null)
            throw new NotFoundException($"Пользователь с Id: {request.Id} не найден");
        return _mapper.Map<UserDto>(user);
    }
}