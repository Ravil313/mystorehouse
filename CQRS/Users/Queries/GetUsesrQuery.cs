﻿using MediatR;

namespace CQRS.Users.Queries;

/// <summary>
/// Модель для получения пользователей
/// </summary>
public class GetUsersQuery : IRequest<IEnumerable<UserDto>>
{ }