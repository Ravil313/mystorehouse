﻿using CQRS.Products.Commands.Models;
using FluentValidation;

namespace CQRS.Products.Commands.Validators;
/// <summary>
/// Валидатор CQRS модели для удаления товара
/// </summary>
public class DeleteProductCommandValidator : AbstractValidator<DeleteProductCommand>
{
    /// <summary>
    /// Правила валидации поля CQRS модели для удаления товара
    /// </summary>
    public DeleteProductCommandValidator()
    {
        RuleFor(pro => pro.ProductArticle)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Номер артикула обязателен и должен быть >= 1");
    }
}