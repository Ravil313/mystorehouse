﻿using CQRS.Products.Commands.Models;
using FluentValidation;

namespace CQRS.Products.Commands.Validators;
/// <summary>
/// Валидатор CQRS модели для создания товара
/// </summary>
public class CreateProductCommandValidator : AbstractValidator<CreateProductCommand>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для создания товара
    /// </summary>
    public CreateProductCommandValidator()
    {
        RuleFor(pro => pro.ProductArticle)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Номер артикула обязателен и должен быть >= 1");
        RuleFor(pro => pro.ProductName)
            .NotEmpty()
            .NotNull()
            .MaximumLength(100)
            .WithMessage("Название товара обязательно и не более 100 символов");
        RuleFor(pro => pro.LastPurchasePrice)
            .GreaterThanOrEqualTo(0)
            .WithMessage("Цена закупки должна быть >= 0");
        RuleFor(pro => pro.LastSellPrice)
            .GreaterThanOrEqualTo(0)
            .WithMessage("Цена продажи должна быть >= 0");
    }
}