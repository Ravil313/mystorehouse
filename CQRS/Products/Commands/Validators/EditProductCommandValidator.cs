﻿using CQRS.Products.Commands.Models;
using FluentValidation;

namespace CQRS.Products.Commands.Validators;
/// <summary>
/// Валидатор CQRS модели редактирования товара
/// </summary>
public class EditProductCommandValidator : AbstractValidator<EditProductCommand>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для редактирования товара
    /// </summary>
    public EditProductCommandValidator()
    {
        RuleFor(pro => pro.ProductArticle)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Номер артикула обязателен и должен быть >= 1");
        RuleFor(pro => pro.NewProductArticle)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Новый номер артикула обязателен и должн быть >= 1");
        RuleFor(pro => pro.NewProductName)
            .NotEmpty()
            .NotNull()
            .MaximumLength(100)
            .WithMessage("Новое название товара обязательно и не более 100 символов");
        RuleFor(pro => pro.NewLastPurchasePrice)
            .GreaterThanOrEqualTo(0)
            .WithMessage("Новая цена закупки должна быть >= 0");
        RuleFor(pro => pro.NewLastSellPrice)
            .GreaterThanOrEqualTo(0)
            .WithMessage("Новая цена продажи должна быть >= 0");
    }
}