﻿using AutoMapper;
using CQRS.Exceptions;
using CQRS.Products.Commands.Models;
using CQRS.Products.Queries;
using DataAccessLayer;
using DataAccessLayer.DataModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Products.Commands.Handlers;
/// <summary>
/// Обработчик CQRS команды создания товара
/// </summary>
public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, int>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public CreateProductCommandHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS команды создания товара
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделью из тела запроса для создания товара</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Идентификатор товара в базе данных</returns>
    public async Task<int> Handle(CreateProductCommand request, CancellationToken cancellationToken)
    {
        var productData = _mapper.Map<ProductData>(request);
        await _dataContext.ProductsData.AddAsync(productData, cancellationToken : cancellationToken);
        await _dataContext.SaveChangesAsync(cancellationToken : cancellationToken);
        return productData.ProductId;
    }
}