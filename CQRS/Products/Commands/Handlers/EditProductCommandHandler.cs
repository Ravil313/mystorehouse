﻿using AutoMapper;
using CQRS.Exceptions;
using CQRS.Products.Commands.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Products.Commands.Handlers;
/// <summary>
/// Обработчик CQRS команды редактирования товара
/// </summary>
public class EditProductCommandHandler : IRequestHandler<EditProductCommand, string>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public EditProductCommandHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS команды редактирования товара
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделью из тела запроса для редактирования товара</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Сообщение о успешном редактировании товара</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если товар не найден/exception></exception>
    public async Task<string> Handle(EditProductCommand request, CancellationToken cancellationToken)
    {
        var productData = await _dataContext.ProductsData
            .FirstOrDefaultAsync(product => product.ProductDataArticle == request.ProductArticle, cancellationToken);
        if (productData == null) throw new NotFoundException($"Продукт с артикулом: {request.ProductArticle} не найден");
        _mapper.Map(request, productData);
        await _dataContext.SaveChangesAsync(cancellationToken);
        return "Продукт изменен";
    }
}