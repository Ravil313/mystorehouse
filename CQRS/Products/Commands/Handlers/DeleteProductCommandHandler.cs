﻿using CQRS.Exceptions;
using CQRS.Products.Commands.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Products.Commands.Handlers;
/// <summary>
/// Обработчик CQRS команды удаления товара
/// </summary>
public class DeleteProductCommandHandler : IRequestHandler<DeleteProductCommand, string>
{
    private readonly DataContext _dataContext;
    /// <summary>
    /// Добавление зависимостей от базы данных
    /// </summary>
    /// <param name="dataContext"></param>
    public DeleteProductCommandHandler(DataContext dataContext)
    {
        _dataContext = dataContext;
    }
    /// <summary>
    /// Метод обработчика CQRS команды удаления товара
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделью из строки запроса для удаления товара</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Сообщение о успешном удалении товара</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если товар не найден</exception>
    public async Task<string> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
    {
        var productData = await _dataContext.ProductsData
            .FirstOrDefaultAsync(product => product.ProductDataArticle == request.ProductArticle, cancellationToken);
        if (productData == null) throw new NotFoundException($"Продукт с артикулом: {request.ProductArticle} не найден");
        _dataContext.Remove(productData);
        await _dataContext.SaveChangesAsync(cancellationToken);
        return "Продукт удален";
    }
}