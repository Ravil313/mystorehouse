﻿using MediatR;

namespace CQRS.Products.Commands.Models;
/// <summary>
/// CQRS модель для удаления товара
/// </summary>
public class DeleteProductCommand : IRequest<string>
{
    /// <summary>
    /// Артикул товара
    /// </summary>
    public int ProductArticle { get; set; }
}