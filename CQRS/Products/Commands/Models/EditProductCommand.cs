﻿using MediatR;

namespace CQRS.Products.Commands.Models;
/// <summary>
/// CQRS модель для редактирования товара
/// </summary>
public class EditProductCommand : IRequest<string>
{
    /// <summary>
    /// Артикул изменяемого товара
    /// </summary>
    public int ProductArticle { get; set; }
    /// <summary>
    /// Новый артикул изменяемого товара
    /// </summary>
    public int NewProductArticle { get; set; }
    /// <summary>
    /// Новое имя изменяемого товара
    /// </summary>
    public string? NewProductName { get; set; }
    /// <summary>
    /// Новая последняя закупочная цена изменяемого товара
    /// </summary>
    public int NewLastPurchasePrice { get; set; }
    /// <summary>
    /// Новая последняя цена продажи изменяемого товара
    /// </summary>
    public int NewLastSellPrice { get; set; }
}