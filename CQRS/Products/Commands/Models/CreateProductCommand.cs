﻿using MediatR;

namespace CQRS.Products.Commands.Models;
/// <summary>
/// CQRS модель для создания товара
/// </summary>
public class CreateProductCommand : IRequest<int>
{
    /// <summary>
    /// Артикул товара
    /// </summary>
    public int ProductArticle { get; set; }
    /// <summary>
    /// Название товара
    /// </summary>
    public string? ProductName { get; set; }
    /// <summary>
    /// Последняя закупочная цена товара
    /// </summary>
    public int LastPurchasePrice { get; set; }
    /// <summary>
    /// Последняя цена продажи товара
    /// </summary>
    public int LastSellPrice { get; set; }
}