﻿using AutoMapper;
using CQRS.Products.Commands.Models;
using DataAccessLayer.DataModels;

namespace CQRS.Products.Commands.Mappers;
/// <summary>
/// Автомаппер CQRS модели для команды редактирования товара
/// </summary>
public class EditProductMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели редактирования товара в таблицу товаров в базе данных
    /// </summary>
    public EditProductMapProfile()
    {
        CreateMap<EditProductCommand, ProductData>()
            .ForMember(productData => productData.ProductDataArticle,
                expression => expression.MapFrom(editProductCommand => editProductCommand.NewProductArticle))
            .ForMember(productData => productData.ProductDataName,
                expression => expression.MapFrom(editProductCommand => editProductCommand.NewProductName))
            .ForMember(productData => productData.LastPurchasePrice,
                expression => expression.MapFrom(editProductCommand => editProductCommand.NewLastPurchasePrice))
            .ForMember(productData => productData.LastSellPrice,
                expression => expression.MapFrom(editProductCommand => editProductCommand.NewLastSellPrice));
    }
}