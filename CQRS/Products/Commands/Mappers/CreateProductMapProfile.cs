﻿using AutoMapper;
using CQRS.Products.Commands.Models;
using DataAccessLayer.DataModels;

namespace CQRS.Products.Commands.Mappers;
/// <summary>
/// Автомаппер CQRS модели для команды создания товара
/// </summary>
public class CreateProductMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели создания товара в таблицу товаров в базе данных
    /// </summary>
    public CreateProductMapProfile()
    {
        CreateMap<CreateProductCommand, ProductData>()
            .ForMember(productData => productData.ProductDataArticle,
                expression => expression.MapFrom(createProductCommand => createProductCommand.ProductArticle))
            .ForMember(productData => productData.ProductDataName,
                expression => expression.MapFrom(createProductCommand => createProductCommand.ProductName))
            .ForMember(productData => productData.LastPurchasePrice,
                expression => expression.MapFrom(createProductCommand => createProductCommand.LastPurchasePrice))
            .ForMember(productData => productData.LastSellPrice,
                expression => expression.MapFrom(createProductCommand => createProductCommand.LastSellPrice));
    }
}