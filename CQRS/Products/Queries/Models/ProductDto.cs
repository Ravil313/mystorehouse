﻿namespace CQRS.Products.Queries.Models;
/// <summary>
/// Модель для получения данных о товаре из базы данных
/// </summary>
public class ProductDto
{
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Артикул товара
    /// </summary>
    public int ProductArticle { get; set; }
    /// <summary>
    /// Название товара
    /// </summary>
    public string? ProductName { get; set; }
    /// <summary>
    /// Последняя закупочная цена товара
    /// </summary>
    public int LastPurchasePrice { get; set; }
    /// <summary>
    /// Последняя цена продажи товара
    /// </summary>
    public int LastSellPrice { get; set; }
    /// <summary>
    /// Список складов в которых храниться товар
    /// </summary>
    public ICollection<StoreHousesDto>? InStoreHouses { get; set; }
}