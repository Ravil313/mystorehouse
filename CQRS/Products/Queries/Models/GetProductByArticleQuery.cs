﻿using MediatR;

namespace CQRS.Products.Queries.Models;
/// <summary>
/// CQRS модель для получения товара по артикулу
/// </summary>
public class GetProductByArticleQuery : IRequest<ProductDto>
{
    /// <summary>
    /// Артикул товара
    /// </summary>
    public int ProductArticle { get; set; } 
}