﻿namespace CQRS.Products.Queries.Models;
/// <summary>
/// Модель для получения данных о складах из базы данных на которых хранится продукт
/// </summary>
public class StoreHousesDto
{
    /// <summary>
    /// Идентификатор склада
    /// </summary>
    public int StoreHouseId { get; set; }
    /// <summary>
    /// Название склада
    /// </summary>
    public string? StoreHouseName { get; set; }
    /// <summary>
    /// Количество товара на складе
    /// </summary>
    public int Count { get; set; }
}