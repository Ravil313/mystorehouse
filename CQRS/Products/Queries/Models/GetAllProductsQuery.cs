﻿using MediatR;

namespace CQRS.Products.Queries.Models;
/// <summary>
/// CQRS Модель для поучения всех товаров
/// </summary>
public class GetAllProductsQuery : IRequest<IEnumerable<ProductDto>>
{ }