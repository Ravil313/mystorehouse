﻿using AutoMapper;
using CQRS.Exceptions;
using CQRS.Products.Queries.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Products.Queries.Handlers;
/// <summary>
/// Обработчик CQRS запроса получения всех товаров
/// </summary>
public class GetAllProductsQueryHandler : IRequestHandler<GetAllProductsQuery, IEnumerable<ProductDto>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetAllProductsQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS запроса получения всех товаров
    /// </summary>
    /// <param name="request">Запрос с запролненной CQRS моделью для получения всех товаров</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Коллекцию товаров из базы данных</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если список товаров пуст</exception>
    public async Task<IEnumerable<ProductDto>> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
    {
        var productData = await _dataContext.ProductsData
            .AsNoTracking()
            .Include(productData => productData.ProductStoreHouseData)!
            .ThenInclude(psh => psh.StoreHouseData)
            .ToListAsync(cancellationToken: cancellationToken);
        if (productData == null)
            throw new NotFoundException($"Список товаров пуст");
        return _mapper.Map<IEnumerable<ProductDto>>(productData);
    }
}