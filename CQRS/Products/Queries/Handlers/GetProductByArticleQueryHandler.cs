﻿using AutoMapper;
using CQRS.Exceptions;
using CQRS.Products.Queries.Models;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CQRS.Products.Queries.Handlers;
/// <summary>
/// Обработчик CQRS запроса получения товара по артикулу
/// </summary>
public class GetProductByArticleQueryHandler : IRequestHandler<GetProductByArticleQuery, ProductDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    /// <summary>
    /// Добавление зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetProductByArticleQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }
    /// <summary>
    /// Метод обработчика CQRS запроса получения товара по артикулу
    /// </summary>
    /// <param name="request">Запрос с заполненной CQRS моделю из строки запроса для получения товара по артикулу</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Модель товара из базы данных</returns>
    /// <exception cref="NotFoundException">Возвращение ошибки если товар не найден</exception>
    public async Task<ProductDto> Handle(GetProductByArticleQuery request, CancellationToken cancellationToken)
    {
        var productData = await _dataContext.ProductsData
            .AsNoTracking()
            .Include(productData => productData.ProductStoreHouseData)!
            .ThenInclude(psh => psh.StoreHouseData)
            .FirstOrDefaultAsync(pd => pd.ProductDataArticle == request.ProductArticle, cancellationToken: cancellationToken);
        if (productData == null)
            throw new NotFoundException($"Товар с артикулом: {request.ProductArticle} не найден");
        return _mapper.Map<ProductDto>(productData);
    }
}