﻿using AutoMapper;
using CQRS.Products.Queries.Models;
using DataAccessLayer.DataModels;

namespace CQRS.Products.Queries.Mappers;
/// <summary>
/// Автомаппер модели CQRS запроса для получения товара из таблицы товаров в базе дыннх
/// </summary>
public class ProductMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга модели CQRS запроса для получения товара из таблицы товаров в базе дыных
    /// </summary>
    public ProductMapProfile()
    {
        CreateMap<ProductData, ProductDto>()
            .ForMember(productDto => productDto.ProductId,expression => expression.MapFrom(productData => productData.ProductId))
            .ForMember(productDto => productDto.ProductArticle,expression => expression.MapFrom(productData => productData.ProductDataArticle))
            .ForMember(productDto => productDto.ProductName,expression => expression.MapFrom(productData => productData.ProductDataName))
            .ForMember(productDto => productDto.LastPurchasePrice,expression => expression.MapFrom(productData => productData.LastPurchasePrice))
            .ForMember(productDto => productDto.LastSellPrice,expression => expression.MapFrom(productData => productData.LastSellPrice))
            .ForMember(productDto => productDto.InStoreHouses,expression => expression.MapFrom(productData => productData.ProductStoreHouseData));
    }
}