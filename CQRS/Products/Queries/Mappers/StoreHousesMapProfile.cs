﻿using AutoMapper;
using CQRS.Products.Queries.Models;
using DataAccessLayer.DataModels;

namespace CQRS.Products.Queries.Mappers;
/// <summary>
/// Автомаппер модели для CQRS запроса получения склада в составе продукта и модели склада в базе дыных в составе таблицы связей
/// </summary>
public class StoreHousesMapProfile : Profile
{
    /// <summary>
    /// Правила маппинга модели для CQRS запроса получения склада в составе продукта и модели склада в базе дыных в составе таблицы связей
    /// </summary>
    public StoreHousesMapProfile()
    {
        CreateMap<ProductStoreHouseData, StoreHousesDto>()
            .ForMember(storeHouseDto => storeHouseDto.StoreHouseId,expression => expression.MapFrom(productStoreHouseData => productStoreHouseData.StoreHouseId))
            .ForMember(storeHouseDto => storeHouseDto.StoreHouseName,expression => expression.MapFrom(productStoreHouseData => productStoreHouseData.StoreHouseData!.StoreHouseName))
            .ForMember(storeHouseDto => storeHouseDto.Count,expression => expression.MapFrom(productStoreHouseData => productStoreHouseData.Count));
    }
}