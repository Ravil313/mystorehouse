﻿using CQRS.Products.Queries.Models;
using FluentValidation;

namespace CQRS.Products.Queries.Validators;
/// <summary>
/// Валидатор CQRS модели для получения товара по артикулу 
/// </summary>
public class GetProductByArticleQueryValidator : AbstractValidator<GetProductByArticleQuery>
{
    /// <summary>
    /// Правила валидации полей CQRS модели для получения товара по артикулу 
    /// </summary>
    public GetProductByArticleQueryValidator()
    {
        RuleFor(pro => pro.ProductArticle)
            .NotEmpty()
            .NotNull()
            .GreaterThanOrEqualTo(1)
            .WithMessage("Номер артикула обязателен и должен быть >= 1");
    }
}