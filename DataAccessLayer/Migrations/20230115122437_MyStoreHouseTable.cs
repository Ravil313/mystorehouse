﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class MyStoreHouseTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdmissionsData",
                columns: table => new
                {
                    AdmissionId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AdmissionNumber = table.Column<int>(type: "integer", nullable: false),
                    StoreHouseId = table.Column<int>(type: "integer", nullable: false),
                    Date = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionsData", x => x.AdmissionId);
                });

            migrationBuilder.CreateTable(
                name: "MovingData",
                columns: table => new
                {
                    MovingId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MovingNumber = table.Column<int>(type: "integer", nullable: false),
                    StoreHouseIdFrom = table.Column<int>(type: "integer", nullable: false),
                    StoreHouseIdTo = table.Column<int>(type: "integer", nullable: false),
                    Date = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovingData", x => x.MovingId);
                });

            migrationBuilder.CreateTable(
                name: "ProductsData",
                columns: table => new
                {
                    ProductId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ProductDataArticle = table.Column<int>(type: "integer", nullable: false),
                    ProductDataName = table.Column<string>(type: "text", nullable: true),
                    LastPurchasePrice = table.Column<int>(type: "integer", nullable: false),
                    LastSellPrice = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductsData", x => x.ProductId);
                });

            migrationBuilder.CreateTable(
                name: "SellsData",
                columns: table => new
                {
                    SellId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SellNumber = table.Column<int>(type: "integer", nullable: false),
                    StoreHouseId = table.Column<int>(type: "integer", nullable: false),
                    Date = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SellsData", x => x.SellId);
                });

            migrationBuilder.CreateTable(
                name: "StoreHousesData",
                columns: table => new
                {
                    StoreHouseId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    StoreHouseName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreHousesData", x => x.StoreHouseId);
                });

            migrationBuilder.CreateTable(
                name: "UsersData",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Login = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    Role = table.Column<int>(type: "integer", nullable: false),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsersData", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionProductsData",
                columns: table => new
                {
                    AdmissionId = table.Column<int>(type: "integer", nullable: false),
                    ProductId = table.Column<int>(type: "integer", nullable: false),
                    Price = table.Column<int>(type: "integer", nullable: false),
                    Count = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionProductsData", x => new { x.ProductId, x.AdmissionId });
                    table.ForeignKey(
                        name: "FK_AdmissionProductsData_AdmissionsData_AdmissionId",
                        column: x => x.AdmissionId,
                        principalTable: "AdmissionsData",
                        principalColumn: "AdmissionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AdmissionProductsData_ProductsData_ProductId",
                        column: x => x.ProductId,
                        principalTable: "ProductsData",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MovingProductsData",
                columns: table => new
                {
                    MovingId = table.Column<int>(type: "integer", nullable: false),
                    ProductId = table.Column<int>(type: "integer", nullable: false),
                    Count = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovingProductsData", x => new { x.ProductId, x.MovingId });
                    table.ForeignKey(
                        name: "FK_MovingProductsData_MovingData_MovingId",
                        column: x => x.MovingId,
                        principalTable: "MovingData",
                        principalColumn: "MovingId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovingProductsData_ProductsData_ProductId",
                        column: x => x.ProductId,
                        principalTable: "ProductsData",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SellProductsData",
                columns: table => new
                {
                    SellId = table.Column<int>(type: "integer", nullable: false),
                    ProductId = table.Column<int>(type: "integer", nullable: false),
                    SellPrice = table.Column<int>(type: "integer", nullable: false),
                    Count = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SellProductsData", x => new { x.ProductId, x.SellId });
                    table.ForeignKey(
                        name: "FK_SellProductsData_ProductsData_ProductId",
                        column: x => x.ProductId,
                        principalTable: "ProductsData",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SellProductsData_SellsData_SellId",
                        column: x => x.SellId,
                        principalTable: "SellsData",
                        principalColumn: "SellId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductsStoreHousesData",
                columns: table => new
                {
                    ProductId = table.Column<int>(type: "integer", nullable: false),
                    StoreHouseId = table.Column<int>(type: "integer", nullable: false),
                    Count = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductsStoreHousesData", x => new { x.ProductId, x.StoreHouseId });
                    table.ForeignKey(
                        name: "FK_ProductsStoreHousesData_ProductsData_ProductId",
                        column: x => x.ProductId,
                        principalTable: "ProductsData",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductsStoreHousesData_StoreHousesData_StoreHouseId",
                        column: x => x.StoreHouseId,
                        principalTable: "StoreHousesData",
                        principalColumn: "StoreHouseId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionProductsData_AdmissionId",
                table: "AdmissionProductsData",
                column: "AdmissionId");

            migrationBuilder.CreateIndex(
                name: "IX_MovingProductsData_MovingId",
                table: "MovingProductsData",
                column: "MovingId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductsStoreHousesData_StoreHouseId",
                table: "ProductsStoreHousesData",
                column: "StoreHouseId");

            migrationBuilder.CreateIndex(
                name: "IX_SellProductsData_SellId",
                table: "SellProductsData",
                column: "SellId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdmissionProductsData");

            migrationBuilder.DropTable(
                name: "MovingProductsData");

            migrationBuilder.DropTable(
                name: "ProductsStoreHousesData");

            migrationBuilder.DropTable(
                name: "SellProductsData");

            migrationBuilder.DropTable(
                name: "UsersData");

            migrationBuilder.DropTable(
                name: "AdmissionsData");

            migrationBuilder.DropTable(
                name: "MovingData");

            migrationBuilder.DropTable(
                name: "StoreHousesData");

            migrationBuilder.DropTable(
                name: "ProductsData");

            migrationBuilder.DropTable(
                name: "SellsData");
        }
    }
}
