﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DataAccessLayer;
/// <summary>
/// Добавление настроек подключения
/// </summary>
public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
{
    /// <summary>
    /// Настройки подключения к базе данных
    /// </summary>
    /// <param name="args"></param>
    /// <returns>Настройки подключения к базе данных</returns>
    public DataContext CreateDbContext(string[] args)
    {
        var optionBuilder = new DbContextOptionsBuilder<DataContext>()
            .UseNpgsql("Host=localhost;Port=5432;Database=MyStoreHouse;Username=postgres;Password=12345");
        return new DataContext(optionBuilder.Options);
    }
}