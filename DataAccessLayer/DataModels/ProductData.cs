﻿namespace DataAccessLayer.DataModels;
/// <summary>
/// Модель таблицы товаров
/// </summary>
public class ProductData
{
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Артикул товара
    /// </summary>
    public int ProductDataArticle { get; set; }
    /// <summary>
    /// Название товара
    /// </summary>
    public string? ProductDataName { get; set; }
    /// <summary>
    /// Последняя закупочная цена товара
    /// </summary>
    public int LastPurchasePrice { get; set; }
    /// <summary>
    /// Последняя цена продажи товара
    /// </summary>
    public int LastSellPrice { get; set; }
    /// <summary>
    /// Коллекция складов в которых хранится товар
    /// </summary>
    public ICollection<ProductStoreHouseData>? ProductStoreHouseData { get; set; }
}