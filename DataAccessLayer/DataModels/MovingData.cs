﻿namespace DataAccessLayer.DataModels;
/// <summary>
/// Модель таблицы документа перемещиния товаров
/// </summary>
public class MovingData
{
    /// <summary>
    /// Идентификатор перемещения
    /// </summary>
    public int MovingId { get; set; }
    /// <summary>
    /// Номер перемещения
    /// </summary>
    public int MovingNumber { get; set; }
    /// <summary>
    /// Идентификатор склада из которого перемещается товар
    /// </summary>
    public int StoreHouseIdFrom { get; set; }
    /// <summary>
    /// Идентификатор склада в который перемещается товар
    /// </summary>
    public int StoreHouseIdTo { get; set; }
    /// <summary>
    /// Дата перемещения
    /// </summary>
    public string? Date { get; set; }
    /// <summary>
    /// Коллекция товаров для перемещения
    /// </summary>
    public ICollection<MovingProductsData>? MovingProducts { get; set; }
}