﻿namespace DataAccessLayer.DataModels;
/// <summary>
/// Модель таблицы связей товара и склада
/// </summary>
public class ProductStoreHouseData
{
   /// <summary>
   /// Идентификатор товара
   /// </summary>
   public int ProductId { get; set; }
   /// <summary>
   /// Идентификатор склада
   /// </summary>
   public int StoreHouseId { get; set; }
   /// <summary>
   /// Навигационное свойство с таблицей товаров
   /// </summary>
   public ProductData? ProductData { get; set; }
   /// <summary>
   /// Навигационное свойство с таблицей складов
   /// </summary>
   public StoreHouseData? StoreHouseData { get; set; }
   /// <summary>
   /// Количество товара
   /// </summary>
   public int Count { get; set; }
}
