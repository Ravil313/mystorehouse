﻿namespace DataAccessLayer.DataModels;
/// <summary>
/// Модель таблицы связи документа поступления товаров и с таблицей товаров
/// </summary>
public class AdmissionProductsData
{
    /// <summary>
    /// Идентификатор поступления
    /// </summary>
    public int AdmissionId { get; set; }
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Закупочная цена товара
    /// </summary>
    public int Price { get; set; }
    /// <summary>
    /// Количество товара
    /// </summary>
    public int Count { get; set; }
    /// <summary>
    /// Навигационное свойство с таблицей поступлений
    /// </summary>
    public AdmissionData? AdmissionData { get; set; }
    /// <summary>
    /// Навигационное свойство с таблицей продуктов
    /// </summary>
    public ProductData? ProductData { get; set; }
}