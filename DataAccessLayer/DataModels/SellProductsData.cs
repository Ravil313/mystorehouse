namespace DataAccessLayer.DataModels;
/// <summary>
/// Модель таблицы связей продаж товара с таблицей товаров
/// </summary>
public class SellProductsData
{
    /// <summary>
    /// Идентификатор продажи
    /// </summary>
    public int SellId { get; set; }
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Цена продажи товара
    /// </summary>
    public int SellPrice { get; set; }
    /// <summary>
    /// Количество товара
    /// </summary>
    public int Count { get; set; }
    /// <summary>
    /// Навигационное свойство с таблицей продаж
    /// </summary>
    public SellData? SellData { get; set; }
    /// <summary>
    /// Навигационное свойство с таблицей продуктов
    /// </summary>
    public ProductData? ProductData { get; set; }
}