﻿namespace DataAccessLayer.DataModels;

/// <summary>
/// Модель пользователя в БД
/// </summary>
public class UserData
{
    /// <summary>
    /// Идентификатор пользователя
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// Логин
    /// </summary>
    public string? Login { get; set; }
    /// <summary>
    /// Пароль
    /// </summary>
    public string? Password { get; set; }
    /// <summary>
    /// Имя пользователя
    /// </summary>
    public string? FirstName { get; set; }
    /// <summary>
    /// Фамилия пользователя
    /// </summary>
    public string? LastName { get; set; }
    /// <summary>
    /// Роль пользователя
    /// </summary>
    public Roles Role { get; set; }
    /// <summary>
    /// Номер телефона пользователя
    /// </summary>
    public string? PhoneNumber { get; set; }
}