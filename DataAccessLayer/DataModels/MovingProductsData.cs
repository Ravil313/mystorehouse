namespace DataAccessLayer.DataModels;
/// <summary>
/// Модель таблицы связей документа пермещения товаров с таблицей товаров
/// </summary>
public class MovingProductsData
{
    /// <summary>
    /// Идентификатор перемещения
    /// </summary>
    public int MovingId { get; set; }
    /// <summary>
    /// Идентификатор товара
    /// </summary>
    public int ProductId { get; set; }
    /// <summary>
    /// Количество товара
    /// </summary>
    public int Count { get; set; }
    /// <summary>
    /// Навигационное свойство с таблицей перемещений
    /// </summary>
    public MovingData? MovingData { get; set; }
    /// <summary>
    /// Навигационное свойство с таблицей продуктов
    /// </summary>
    public ProductData? ProductData { get; set; }
}