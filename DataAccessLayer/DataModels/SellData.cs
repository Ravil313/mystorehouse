﻿namespace DataAccessLayer.DataModels;
/// <summary>
/// Модель таблицы документа продажи товаров
/// </summary>
public class SellData
{
    /// <summary>
    /// Идентификатор продажи
    /// </summary>
    public int SellId { get; set; }
    /// <summary>
    /// Номер продажи
    /// </summary>
    public int SellNumber { get; set; }
    /// <summary>
    /// Идентификатор склада
    /// </summary>
    public int StoreHouseId { get; set; }
    /// <summary>
    /// Дата продажи
    /// </summary>
    public string? Date { get; set; }
    /// <summary>
    /// Коллекция проданных товаров
    /// </summary>
    public ICollection<SellProductsData>? SellProducts { get; set; }
}