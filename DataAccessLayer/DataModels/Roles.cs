﻿namespace DataAccessLayer.DataModels;

/// <summary>
/// Роли доступа для пользователей
/// </summary>
public enum Roles
{
    /// <summary>
    /// Гость - только просмотр
    /// </summary>
    Guest,
    /// <summary>
    /// Пользователь - просмотр и создание
    /// </summary>
    User,
    /// <summary>
    /// Администратор - полный доступ
    /// </summary>
    Admin
}