﻿namespace DataAccessLayer.DataModels;
/// <summary>
/// Модель таблицы складов
/// </summary>
public class StoreHouseData
{
    /// <summary>
    /// Идентификатор скалда
    /// </summary>
    public int StoreHouseId { get; set; }
    /// <summary>
    /// Название скалада
    /// </summary>
    public string? StoreHouseName { get; set; }
    /// <summary>
    /// Коллекция товаров хранящихся на складе
    /// </summary>
    public ICollection<ProductStoreHouseData>? ProductStoreHouseData { get; set; }
}