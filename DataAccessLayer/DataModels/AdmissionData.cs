﻿namespace DataAccessLayer.DataModels;
/// <summary>
/// Модель таблицы документа поступления товаров
/// </summary>
public class AdmissionData
{
    /// <summary>
    /// Идентификатор поступления
    /// </summary>
    public int AdmissionId { get; set; }
    /// <summary>
    /// Номер поступления
    /// </summary>
    public int AdmissionNumber { get; set; }
    /// <summary>
    /// Идентификатор склада для поступления
    /// </summary>
    public int StoreHouseId { get; set; }
    /// <summary>
    /// Дата поступления
    /// </summary>
    public string? Date { get; set; }
    /// <summary>
    /// Коллекция товаров в поступлениии
    /// </summary>
    public ICollection<AdmissionProductsData>? AdmissionProducts { get; set; }
}
