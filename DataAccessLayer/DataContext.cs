﻿using DataAccessLayer.Configurations;
using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer;
/// <summary>
/// Создание таблиц
/// </summary>
public class DataContext : DbContext
{
    /// <summary>
    /// Таблица товаров
    /// </summary>
    public DbSet<ProductData> ProductsData { get; set; }
    /// <summary>
    /// Таблица складов
    /// </summary>
    public DbSet<StoreHouseData> StoreHousesData { get; set; }
    /// <summary>
    /// Таблица связей товара и склада
    /// </summary>
    public DbSet<ProductStoreHouseData> ProductsStoreHousesData { get; set; }
    /// <summary>
    /// Таблица поступлений
    /// </summary>
    public DbSet<AdmissionData> AdmissionsData { get; set; }
    /// <summary>
    /// Таблица связей поступления и товаров
    /// </summary>
    public DbSet<AdmissionProductsData> AdmissionProductsData { get; set; }
    /// <summary>
    /// Таблица перемещений
    /// </summary>
    public DbSet<MovingData> MovingData { get; set; }
    /// <summary>
    /// Таблица связей перемещения и товаров
    /// </summary>
    public DbSet<MovingProductsData> MovingProductsData { get; set; }
    /// <summary>
    /// Таблица продаж
    /// </summary>
    public DbSet<SellData> SellsData { get; set; }
    /// <summary>
    /// Таблица связей продажи и товаров
    /// </summary>
    public DbSet<SellProductsData> SellProductsData { get; set; }
    /// <summary>
    /// Таблица пользователей
    /// </summary>
    public DbSet<UserData> UsersData { get; set; }
    /// <summary>
    /// Добавление миграции
    /// </summary>
    /// <param name="options">Настройки подключения к базе данных</param>
    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
        Database.Migrate();
    }
    /// <summary>
    /// Создание таблиц в базе данных на основе миграций
    /// </summary>
    /// <param name="modelBuilder"></param>
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(ProductsDataConfig).Assembly);
    }
}