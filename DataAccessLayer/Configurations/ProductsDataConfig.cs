﻿using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;
/// <summary>
/// Конфигурация таблицы товаров
/// </summary>
public class ProductsDataConfig : IEntityTypeConfiguration<ProductData>
{
    /// <summary>
    /// Конфигурация таблицы товаров
    /// </summary>
    public void Configure(EntityTypeBuilder<ProductData> builder)
    {
        builder.HasKey(productData => productData.ProductId);
    }
}