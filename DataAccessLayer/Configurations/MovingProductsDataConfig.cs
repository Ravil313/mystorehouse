﻿using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;
/// <summary>
/// Конфигурация таблицы связей перемещения товара
/// </summary>
public class MovingProductsDataConfig : IEntityTypeConfiguration<MovingProductsData>
{
    /// <summary>
    /// Конфигурация таблицы связей перемещения товара
    /// </summary>
    public void Configure(EntityTypeBuilder<MovingProductsData> builder)
    {
        builder.HasKey(admissionProductsData => new { admissionProductsData.ProductId, admissionProductsData.MovingId });
        builder.HasOne(movingProductsData => movingProductsData.MovingData)
            .WithMany(productData => productData.MovingProducts);
    }
}