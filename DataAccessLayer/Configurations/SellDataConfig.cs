﻿using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;
/// <summary>
/// Конфигурация таблицы продаж товара
/// </summary>
public class SellDataConfig : IEntityTypeConfiguration<SellData>
{
    /// <summary>
    /// Конфигурация таблицы продаж товара
    /// </summary>
    public void Configure(EntityTypeBuilder<SellData> builder)
    {
        builder.HasKey(admissionData => admissionData.SellId);
    }
}