﻿using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;
/// <summary>
/// Конфигурация таблицы поступления товара
/// </summary>
public class AdmissionDataConfig : IEntityTypeConfiguration<AdmissionData>
{
    /// <summary>
    /// Правила конфигурации таблицы поступления товара
    /// </summary>
    /// <param name="builder"></param>
    public void Configure(EntityTypeBuilder<AdmissionData> builder)
    {
        builder.HasKey(admissionData => admissionData.AdmissionId);
    }
}