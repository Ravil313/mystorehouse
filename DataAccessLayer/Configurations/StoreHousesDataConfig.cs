﻿using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;
/// <summary>
/// Конфигурация таблицы складов
/// </summary>
public class StoreHousesDataConfig : IEntityTypeConfiguration<StoreHouseData>
{
    /// <summary>
    /// Правила конфигурации таблицы складов
    /// </summary>
    public void Configure(EntityTypeBuilder<StoreHouseData> builder)
    {
        builder.HasKey(storeHouseData => storeHouseData.StoreHouseId);
    }
}