﻿using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;
/// <summary>
/// Конфигурация таблицы связей поступления товара
/// </summary>
public class AdmissionProductsDataConfig : IEntityTypeConfiguration<AdmissionProductsData>
{
    /// <summary>
    /// Конфигурация таблицы связей поступления товара
    /// </summary>
    public void Configure(EntityTypeBuilder<AdmissionProductsData> builder)
    {
        builder.HasKey(admissionProductsData => new { admissionProductsData.ProductId, admissionProductsData.AdmissionId });
        builder.HasOne(admissionProductsData => admissionProductsData.AdmissionData)
            .WithMany(productData => productData.AdmissionProducts);
    }
}