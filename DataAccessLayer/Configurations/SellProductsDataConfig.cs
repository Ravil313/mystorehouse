﻿using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;
/// <summary>
/// Конфигурация таблицы связей продажи и товара
/// </summary>
public class SellProductsDataConfig : IEntityTypeConfiguration<SellProductsData>
{
    /// <summary>
    /// Правила конфигурация таблицы связей продажи и товара
    /// </summary>
    public void Configure(EntityTypeBuilder<SellProductsData> builder)
    {
        builder.HasKey(admissionProductsData => new { admissionProductsData.ProductId, admissionProductsData.SellId });
        builder.HasOne(sellProductsData => sellProductsData.SellData)
            .WithMany(productData => productData.SellProducts);
    }
}