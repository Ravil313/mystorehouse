﻿using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;
/// <summary>
/// Конфигурация таблицы перемещений товара
/// </summary>
public class MovingDataConfig : IEntityTypeConfiguration<MovingData>
{
    /// <summary>
    /// Конфигурация таблицы перемещений товара
    /// </summary>
    public void Configure(EntityTypeBuilder<MovingData> builder)
    {
        builder.HasKey(admissionData => admissionData.MovingId);
    }
}