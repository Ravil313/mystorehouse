﻿using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;
/// <summary>
/// Конфигурация таблицы связей товара и склада
/// </summary>
public class ProductStoreHouseDataConfig : IEntityTypeConfiguration<ProductStoreHouseData>
{
    /// <summary>
    /// Конфигурация таблицы связей товара и склада
    /// </summary>
    public void Configure(EntityTypeBuilder<ProductStoreHouseData> builder)
    {
        builder.HasKey(psh => new { psh.ProductId, psh.StoreHouseId });
        builder.HasOne(psd => psd.ProductData)
            .WithMany(pd => pd.ProductStoreHouseData);
        builder.HasOne(psd => psd.StoreHouseData)
            .WithMany(sd => sd.ProductStoreHouseData);
    }
}